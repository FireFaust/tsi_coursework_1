#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "structprocessor.h"
#include "ostreamcolor/modifier.h"
#include "helper.h"

class FileManager
{
public:
    FileManager();
	~FileManager();
    void setDataToProcess(int argc, char**argv);
private:
	StructProcessor *groupProcessor;
	StructProcessor *albumProcessor;
	StructProcessor *songProcessor;

    bool checkData(int argc,char **);

    void receiveDataFromProcessor(ushort &idToInsert, char* name, const char* tableName);
    void prepareIdsForDeletion(const char* tableName, ushort &ids, ushort);
    void deleteEntriesByID(const char*tableName, ushort id);
	void prepareForeignIdsToStack(char*,char *,char *, char*, std::vector<ushort> &);

    Helper help;
};

#endif // FILEMANAGER_H
