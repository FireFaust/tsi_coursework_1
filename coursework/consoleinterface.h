#ifndef CONSOLEINTERFACE_H
#define CONSOLEINTERFACE_H

#include "ostreamcolor/modifier.h"

#include <deque>
#include <string>

#include <functional>

class ConsoleInterface
{
public:
    ConsoleInterface();
    ConsoleInterface(int argc, char *argv[]);
	virtual ~ConsoleInterface();
public:
	virtual void startReading() = 0;
    virtual void stopReading() = 0;
	/**
	* @brief setCallbackFunction
	* Function receive value from dispatcher
	* calls dispatcher function
	*/
    virtual void setCallbackFunction(std::function<void(std::string)> func) = 0;
protected:
    std::string terminalType;
    typedef enum{
        NO_KEY_PRESSED,
        LIST_TO_END,
        LIST_TO_START
    }LAST_COMMAND;

    virtual void init() = 0;
    virtual void giveCommand(std::string command) = 0;
    virtual void printInfo() = 0;
    virtual void printOptions() = 0;

    LAST_COMMAND command_state;
    std::deque<std::string> commandDeq;
    unsigned int commandDeqElementPositionStatus;
    std::pair<unsigned int,std::deque<char> > backspaceChars;
    const std::string _exit = "--exit";

};

#endif // CONSOLEINTERFACE_H
