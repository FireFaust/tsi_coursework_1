#include "consolereader.h"

#include <iostream>
#include <sstream> ///for thread id

#define priority_cout std::cout << Color::FG_CYAN <<  "High priority comand received: "
#define system_cout std::cout << Color::FG_YELLOW

#if defined(UNIX) || defined(__unix__) || defined(LINUX) || defined(__linux__) || defined(__linux)
    const char enterSym = 10;
	const char backspace = 127;
	const char firstSymToCatch = '\033';
	const char UP_ARROW = 'A';
	const char DOWN_ARROW = 'B';
	const char LEFT_ARROW = 'D';
	const char RIGHT_ARROW = 'C';
	const char DELETE_KEY = '3';
	#define secondCheck_DELETE_KEY getch() == 126
#elif defined(_WIN64) || defined(_WIN32)
    const char enterSym = 13;
	const char backspace = 8;
	const char firstSymToCatch = -32;
	const char UP_ARROW = 72;
	const char DOWN_ARROW = 80;
	const char LEFT_ARROW = 75;
	const char RIGHT_ARROW = 77;
	const char DELETE_KEY = 83;
	#define secondCheck_DELETE_KEY true
#endif


ConsoleReader *ConsoleReader::instance = NULL;

std::thread::id main_thread_id;

bool ext = false;

ConsoleReader::ConsoleReader()
{
    init();
}

ConsoleReader::ConsoleReader(int argc, char *argv[])
{
    init();
}

ConsoleReader::~ConsoleReader()
{

}

void * ConsoleReader::infiniteReader()
{
    instance->printInfo();
	while (!ext)
	{
		char ch = getch();
		//std::cout << (int)ch;
		switch (ch)
		{
		case firstSymToCatch:
			#if defined(UNIX) || defined(__unix__) || defined(LINUX) || defined(__linux__) || defined(__linux)
						getch();
			#endif
            switch (getch()) { /// DO NOT FORGET TO IMPROVE QUALITY - i can avoid unsigned itn using for calculation
			case UP_ARROW:  ///GET COMMAND FROM LIST (FROM BEGIN TO END)
				if (instance->commandDeq.size() > instance->commandDeqElementPositionStatus) {
					if (instance->command_state == LIST_TO_START)
						instance->commandDeqElementPositionStatus = 0;
					instance->command_state = LIST_TO_END;
					std::cout << "\r";
					for (unsigned int var = 0; var < instance->backspaceChars.second.size(); ++var) {
						std::cout << " ";
					}
					std::cout << "\r";
					std::cout << instance->commandDeq[instance->commandDeqElementPositionStatus];
					instance->command = instance->commandDeq[instance->commandDeqElementPositionStatus];
					instance->backspaceChars.second.clear();
					for (unsigned int var = 0; var < instance->command.size(); ++var) {
						instance->backspaceChars.second.push_back(instance->command[var]);
					}
					instance->backspaceChars.first = 0;
					instance->commandDeqElementPositionStatus++;
				}
				break;
			case DOWN_ARROW:   ///GET COMMAND FROM LIST (FROM CURRENT POSITION TO BEGIN)
				std::cout << "\r";
				for (unsigned int var = 0; var < instance->backspaceChars.second.size(); ++var) {
					std::cout << " ";
				}
				std::cout << "\r";
				if ((instance->commandDeqElementPositionStatus >= 1) && ///need to comment that statement
					(!(instance->commandDeqElementPositionStatus == 1 && instance->command_state == LIST_TO_END))) {
					if (instance->command_state == LIST_TO_END)
						instance->commandDeqElementPositionStatus--;
					instance->commandDeqElementPositionStatus--;
					std::cout << instance->commandDeq[instance->commandDeqElementPositionStatus];
					instance->command = instance->commandDeq[instance->commandDeqElementPositionStatus];
					instance->backspaceChars.second.clear();
					for (unsigned int var = 0; var < instance->command.size(); ++var) {
						instance->backspaceChars.second.push_back(instance->command[var]);
					}
					instance->backspaceChars.first = 0;
					instance->command_state = LIST_TO_START;
				}
				else
				{
					if (instance->commandDeqElementPositionStatus)
						instance->commandDeqElementPositionStatus--;
					instance->command = "";
					instance->backspaceChars.first = 0;
					instance->backspaceChars.second.clear();
				}
				break;
			case LEFT_ARROW:  ///LEFT
				std::cout << "\b";
				if (instance != NULL) {
					if (instance->backspaceChars.first == 0 && instance->backspaceChars.second.size()) {
						instance->backspaceChars.first = instance->backspaceChars.second.size();
					}
					else
						/// should be more than 1
						if (instance->backspaceChars.first > 1)
							instance->backspaceChars.first--;
				}
				break;
			case RIGHT_ARROW:  ///RIGHT
				if ((instance != NULL)) {
					if (instance->backspaceChars.first <= instance->backspaceChars.second.size() && instance->backspaceChars.second.size() && instance->backspaceChars.first) {
						std::cout << instance->backspaceChars.second.at(instance->backspaceChars.first - 1);
						instance->backspaceChars.first++;
						/// ACHTUNG! LOOKS LIKE BAD STYLE --->
						if (instance->backspaceChars.first > instance->backspaceChars.second.size())
							instance->backspaceChars.first = 0;
						/// <---
						//instance->backspaceChars.second.pop_front();
						//std::cout << std::endl << instance->backspaceChars.second.size() << " " << instance->backspaceChars.first << std::endl;
					}
				}
				break;
			case DELETE_KEY:  ///DELETE
				if (secondCheck_DELETE_KEY)
				{
					if (instance != NULL)
					{
						if (instance->backspaceChars.second.size() && instance->backspaceChars.first)
						{
							/// delete one highlighted symbol
							instance->command = "";
							std::deque<char> tDeq;
							/// ---> clears line
							std::cout << "\r";
							for (std::deque<char>::iterator it = instance->backspaceChars.second.begin(); it < instance->backspaceChars.second.end(); ++it) {
								std::cout << " ";
							}
							std::cout << "\r";
							/// <---
							/// ---> inserts all chars exept deleted one to temporary container;
							unsigned int i = 0;
							for (std::deque<char>::iterator it = instance->backspaceChars.second.begin(); it != instance->backspaceChars.second.end(); ++it) {
								if (tDeq.size()) {
									if (i != instance->backspaceChars.first - 1) {
										tDeq.push_back(*(it));
										instance->command += *(it);
										std::cout << *(it);
									}
								}
								else {
									if (i != instance->backspaceChars.first - 1)
									{
										tDeq.push_back((*it));
										std::cout << *(it);
										instance->command += *(it);
									}
								}
								i++;
							}
							/// <---
							/// ---> rewrite line and stop cursor at higlighted position
							std::cout << "\r";
							for (unsigned int i = 0; i < instance->backspaceChars.first - 1; ++i) {
								if (tDeq.size() > i)
									std::cout << tDeq.at(i);
							}
							/// <---
							/// ---> replace main container
							instance->backspaceChars.second = tDeq;
							//instance->backspaceChars.first--;
							/// <---
						}
					}
				}
				break;
			default:
				break;
			}
			break;
		case enterSym:
			std::cout << std::endl;
			if (instance != NULL)
			{			
				if (instance->command.size()) {
					if (instance != NULL) {
						instance->giveCommand(instance->command);
						instance->backspaceChars.first = 0;
						instance->backspaceChars.second.clear();
                        if (instance->commandDeq.size() >= instance->options->getHistorySize())
							instance->commandDeq.pop_back();
                        instance->commandDeq.push_front(instance->command);
						instance->commandDeqElementPositionStatus = 0;
					}
					instance->command = "";
				}
			}
			break;
		case backspace:
			if (instance != NULL)
			{
				if (instance->backspaceChars.first)
				{
					/// delete one symbol before highlighted symbol
                    if (instance->backspaceChars.first != 1) {
						instance->command = "";
						std::deque<char> tDeq;
						/// ---> clears line
						std::cout << "\r";
						for (std::deque<char>::iterator it = instance->backspaceChars.second.begin(); it < instance->backspaceChars.second.end(); ++it) {
							std::cout << " ";
						}
						std::cout << "\r";
						/// <---
						/// ---> inserts all chars exept deleted one to temporary container;
						unsigned int i = 0;
						for (std::deque<char>::iterator it = instance->backspaceChars.second.begin(); it != instance->backspaceChars.second.end(); ++it) {
							if (tDeq.size()) {
                                if (i != instance->backspaceChars.first - 2) {
									tDeq.push_back(*(it));
									instance->command += *(it);
									std::cout << *(it);
								}
							}
                            else {
                                if(i!= instance->backspaceChars.first-2){   ///do not sure if it it necessary
                                    tDeq.push_back((*it));
                                    std::cout << *(it);
                                    instance->command += *(it);
                                }
							}
							i++;
						}
						/// <---
						/// ---> print new line
						std::cout << "\r";
                        for (unsigned int i = 0; i < instance->backspaceChars.first-2; ++i) {
							std::cout << tDeq.at(i);
						}
						/// <---
						/// ---> replace main container
						instance->backspaceChars.second = tDeq;
						instance->backspaceChars.first--;
						/// <---
					}
				}
				else
				{
					std::cout << "\b";
					std::cout << " ";
					std::cout << "\b";
					if (instance->command.size())
						instance->command.pop_back();
					if (instance->backspaceChars.second.size())
						instance->backspaceChars.second.pop_back();
				}
			}
			break;
		default:
			if (instance != NULL)
			{
				if (!instance->backspaceChars.first)
				{
					std::cout << ch;
					instance->backspaceChars.second.push_back(ch);
					instance->command += ch;
				}
				else
				{
					instance->command = "";
					std::deque<char> tDeq;
					/// ---> clears line
					std::cout << "\r";
					for (std::deque<char>::iterator it = instance->backspaceChars.second.begin(); it < instance->backspaceChars.second.end(); ++it) {
						std::cout << " ";
					}
					std::cout << "\r";
					/// <---
					/// ---> inserts all chars exept new one to temporary container;
					unsigned int i = 0;
					for (std::deque<char>::iterator it = instance->backspaceChars.second.begin(); it != instance->backspaceChars.second.end(); ++it) {
						if (tDeq.size()) {
							if (i != instance->backspaceChars.first - 1) {
								tDeq.push_back(*(it));
								instance->command += *(it);
								std::cout << *(it);
							}
							else
							{
								tDeq.push_back(ch);
								instance->command += ch;
								std::cout << ch;
								tDeq.push_back(*(it));
								instance->command += *(it);
								std::cout << *(it);
							}
						}
						else {
							if (!(instance->backspaceChars.first - 1))
							{
								tDeq.push_back(ch);
								instance->command += ch;
								std::cout << ch;
							}
							tDeq.push_back((*it));
							instance->command += *(it);
							std::cout << *(it);
						}
						i++;
					}
					/// <---
					/// ---> print new line
					std::cout << "\r";
					for (unsigned int i = 0; i < instance->backspaceChars.first; ++i) {
						///ACHTUNG!!!! SHOUD REPAIR!
						//if(tDeq.size()<i)
						try
						{
							std::cout << tDeq.at(i);
						}
						catch (...) {
							std::cout << tDeq.size() << " " << instance->backspaceChars.first << " " << instance->backspaceChars.second.size() << std::endl;
						}
					}
					/// <---
					/// ---> replace main container
					instance->backspaceChars.first++;
					instance->backspaceChars.second = tDeq;
					tDeq.clear();
					/// <---
				}
			}
			else {
				system_cout << Color::ST_BOLD << "CRITICAL ERROR, NO INSTANCE AVAILABLE" << std::endl;
				ext = true;
				//std::cout << ch;
				//instance->command += ch;
			}
			break;
		}
	}
	/// only for Visual Studio
	return NULL;
}

void ConsoleReader::giveCommand(std::string command)
{
    if(command.find(_exit)!= std::string::npos){
        if(command==_exit){
            priority_cout << Color::FG_RED << _exit << Color::FG_DEFAULT << std::endl;
            system_cout << Color::ST_BOLD<< "terminating system" << Color::RESET<< std::endl;
            ext = true;
            system_cout << Color::ST_BOLD << "done" << Color::RESET << std::endl;
        }
        else
		{
			system_cout << Color::ST_BOLD << "exit command should be alone" << Color::RESET << std::endl;
		}
    }
	else
	{
		dispatcherCallbackPtr(command);
	}
}

void ConsoleReader::init()
{
    instance = this;
    instance->command = "";
    backspaceChars.first = 0;
    commandDeqElementPositionStatus = 0;
    command_state = NO_KEY_PRESSED;
#if defined(UNIX) || defined(__unix__) || defined(LINUX) || defined(__linux__) || defined(__linux)
    //terminalType = "LINUX TERMINAL: THREAD ID ";
    options = new TerminalOptions(const_cast<char*>("LINUX TERMINAL"));
#elif defined(_WIN64) || defined(_WIN32)
    //terminalType = "WINDOWS TERMINAL: THREAD ID ";
    options = new TerminalOptions(const_cast<char*>("WINDOWS TERMINAL"));
#endif
	options->setHistorySize(1000);
}

void ConsoleReader::startReading()
{    
    terminalThread = std::thread(infiniteReader);
    std::stringstream ss;
    ss << terminalThread.get_id();
    uint64_t id = std::stoull(ss.str());
    options->setThreadID(id);
    terminalThread.join();
}

void ConsoleReader::stopReading()
{
    std::cout << "ConsoleReader::stop()" << std::endl;
    /// temp
    ext = true;
}

void ConsoleReader::printInfo()
{
    std::cout << instance->options->getType() << ": THREAD ID " << instance->options->getThreadID()<< Color::FG_GREEN << " INITIALISED" << Color::FG_DEFAULT << std::endl;
}

void ConsoleReader::printOptions()
{
    system_cout << "Thread settings" <<  Color::FG_DEFAULT << std::endl;
}
