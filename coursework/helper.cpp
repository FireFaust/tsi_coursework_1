#include "helper.h"

#include "ostreamcolor/modifier.h"

#include <iostream>

Helper::Helper()
{

}
//delete --song -name name -what song -all 1 -delOpt 0 -container "TAAA"
//show --song -what year -condition > -compare 1980 -container group
Helper::~Helper()
{

}

void Helper::printHelp()
{
    std::cout << "Printing Help " << std::endl;
    std::cout << "_S - means String " << "_i - means integer" << std::endl;
    std::cout << "To " << Color::FG_CYAN << "insert " << Color::RESET << "data, type: " << std::endl;
    std::cout << "\t"  << "insert    - main keyword for insertion" << std::endl;
    std::cout << "\t\t" << "<--group>" << std::endl;
    std::cout << "\t\t\t" << "<-name> groupname_S" <<std::endl;
    std::cout << "\t\t\t" << "<-year> year_i" << std::endl;
    std::cout << "\t\t\t" << "<-country> country_S" << std::endl;
    std::cout << "\t\t\t" << "<-genre> genre_S" << std::endl;
    std::cout << "\t\t" <<  "<--album> " << std::endl;
    std::cout << "\t\t\t" << "<-name> albumname_S" << std::endl;
    std::cout << "\t\t\t"<<"<-year> year_i" << std::endl;
    std::cout << "\t\t\t"<<"<-count> count_i" << std::endl;
    std::cout << "\t\t" <<  "<--song>" << std::endl;
    std::cout << "\t\t\t" << "<-name> songpname_S" << std::endl;
    std::cout << "\t\t\t" << "<-duration> duration_i " << std::endl;
    std::cout << "To " << Color::FG_CYAN << "print " << Color::RESET << "data, type: " << std::endl;
    std::cout << "\t"  << "print    - main keyword for printing" << std::endl;
    std::cout << "\t\t" <<  "<--file> <--group> | <--album> | <--song> | <--ALL>" << std::endl;
    std::cout << "\t\t" <<  "<--memory> ACHTUNG! currently not supported" << std::endl;
    std::cout << "To " << Color::FG_CYAN << "sort " << Color::RESET << "data, type: " << std::endl;
    std::cout << "\t"  << "sort    - main keyword for sorting(sorting change files too)" << std::endl;
    std::cout << "\t\t" <<  "(<--group> | <--album> | <--song>)" << std::endl;
    std::cout << "\t\t\t" << "<-what> (<string> | <int>" << std::endl;
    std::cout << "\t\t\t" << "<-where> columnname_S" << std::endl;
    std::cout << "\t\t\t"<< "<-desc> 1/0" << std::endl;
    std::cout << "To " << Color::FG_CYAN << "edit " << Color::RESET << "data, type: " << std::endl;
    std::cout << "\t"  << "edit    - main keyword for editing" << std::endl;
    std::cout << "\t\t" <<  "<-what>" << std::endl;
    std::cout << "\t\t  " <<  "name of object(name)" << std::endl;
    std::cout << "\t\t" <<  "<--group>" << std::endl;
    std::cout << "\t\t\t" << "<-name> groupname_S" << std::endl;
    std::cout << "\t\t\t" << "<-year> year_i" << std::endl;
    std::cout << "\t\t\t" << "<-country> country_S" << std::endl;
    std::cout << "\t\t\t" << "<-genre> genre_S" << std::endl;
    std::cout << "\t\t" <<  "<--album>" << std::endl;
    std::cout << "\t\t\t" << "<-name> albumname_S" << std::endl;
    std::cout << "\t\t\t" << "<-year> year_i" <<std::endl;
    std::cout << "\t\t\t" << "<-count> count_i" << std::endl;
    std::cout << "\t\t" <<  "<--song>" << std::endl;
    std::cout << "\t\t\t" << "<-name> songpname_S" << std::endl;
    std::cout << "\t\t\t" << "<-duration> duration_i " << std::endl;
    std::cout << "\t\t\t" << "<-container> albumname_S " << std::endl;
    std::cout << "To " << Color::FG_CYAN << "delete " << Color::RESET << "data, type: " << std::endl;
    std::cout << "\t"  << "delete    - main keyword for delete operation" << std::endl;
    std::cout << "\t  " <<  "(<--group> | <--album> | <--song>) " << std::endl;
    std::cout << "\t  " <<  "<-column> columnname_S" << std::endl;
    std::cout << "\t      " <<  "only name allowed" << std::endl;
    std::cout << "\t  " <<  "<-what> name_of_object_to delete" << std::endl;
    std::cout << "\t  " <<  "<-container> (for songs and albums)" << std::endl;
    std::cout << "\t    " <<  "<-container> where object belongs (what album contains this song?)" << std::endl;
    std::cout << "\t  " <<  "<-all> 1/0" << std::endl;
    std::cout << "\t    " << "<-all> (for groups and albums) if 1" << std::endl;
    std::cout << "\t    " <<  "delete all data bounded with that object" << std::endl;
    std::cout << "To " << Color::FG_CYAN << "show " << Color::RESET << "data, type: " << std::endl;
    std::cout << "\t"  << "show    - main keyword for showing" << std::endl;
    std::cout << "\t  " <<  "(<--group> | <--album> | <--song>)" << std::endl;
    std::cout << "\t  " <<  "<-what> columnname_S" << std::endl;
    std::cout << "\t    " <<  "<-what> it is column name for searching" << std::endl;
    std::cout << "\t  " <<  "<-condition> sondition_S" << std::endl;
    std::cout << "\t    " <<  "<-condition> it is: = != < >= < <=" << std::endl;
    std::cout << "\t  " <<  "<-compare> comp_S_i" << std::endl;
    std::cout << "\t    " <<  "<-compare> value to compare with (can be int or string" << std::endl;
    std::cout << "\t  " <<  "<-container> (for songs and albums) <-all> 1/0" << std::endl;
    std::cout << "\t    " <<  "<-container> - for crosstable searching - container name" << std::endl;
}
