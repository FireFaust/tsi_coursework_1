#ifndef STRUCTS
#define STRUCTS

#include <ostream>
#include <iomanip>

typedef  unsigned short ushort;


/**
 * @brief The BaseStruct struct
 * @param ID no user required - system choose atomatically. UNICAL
 * @param _name provided by user. Should be UNICAL
 */
struct BaseStruct{
    ushort ID;
    char _name[30];
};

inline std::ostream& operator<<(std::ostream& os, const BaseStruct& dt)
{
    os << std::setw(5) << dt.ID << std::setw(20) << dt._name;
	return os;
}

struct GroupStruct : BaseStruct
{
    char _country[20];
    char _genre[20];
    ushort _year;
};

inline std::ostream& operator<<(std::ostream& os, const GroupStruct& dt)
{
    os << std::setfill(' ') << std::setw(5) << dt.ID
       << "|"<< std::setw(20) << dt._name
       << "|"<< std::setw(20) << dt._country
       << "|"<< std::setw(20) << dt._genre
       << "|"<< std::setw(5) << dt._year << "|"<< "\n";
    os << std::setfill('-') << std::setw(6) << "+" << std::setfill('-') << std::setw(21) << "+"
       << std::setfill('-') << std::setw(21) << "+"<< std::setfill('-') << std::setw(21) << "+"
       << std::setfill('-') << std::setw(6) << "+";
	return os;
}

struct AlbumStruct : BaseStruct{
    ushort _foreignID;
    ushort _count;
    ushort _year;
};

//print --file --ALL

inline std::ostream& operator<<(std::ostream& os, const AlbumStruct& dt)
{
    os << std::setfill(' ') <<std::setw(5)<< dt.ID
        << "|"<< std::setw(20) << dt._name
        << "|"<< std::setw(10) << dt._count
        << "|"<< std::setw(10) << dt._year
        << "|"<< std::setw(5) << dt._foreignID << "|" << "\n";
    os << std::setfill('-') << std::setw(6) << "+" << std::setfill('-') << std::setw(21) << "+"
       << std::setfill('-') << std::setw(11) << "+" << std::setfill('-') << std::setw(11) <<"+"
       << std::setfill('-') << std::setw(6) << "+";
    return os;
}

struct SongStruct : BaseStruct{
    ushort _foreignID;
    ushort _duration;
};

inline std::ostream& operator<<(std::ostream& os, const SongStruct& dt)
{
    os << std::setfill(' ') <<std::setw(5)<< dt.ID << "|" << std::setw(20) << dt._name
        << "|"<< std::setw(10) << dt._duration
        << "|"<< std::setw(5) << dt._foreignID << "|"<< "\n";
    os << std::setfill('-') << std::setw(6) << "+" << std::setfill('-') << std::setw(21) << "+"
       << std::setfill('-') << std::setw(11) << "+"<< std::setfill('-') << std::setw(6) << "+";
    return os;
}

#endif // STRUCTS

