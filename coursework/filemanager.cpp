#include "filemanager.h"
#include <iostream>

#include <cstring>

FileManager::FileManager()
{
	groupProcessor = new StructProcessor(StructType::GROUP_STRUCT);

    std::function<void(ushort &, char*, const char*)> demandForeignID;
    demandForeignID = std::bind(&FileManager::receiveDataFromProcessor, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    groupProcessor->setCallbackFunction(demandForeignID);

    std::function<void(const char*,ushort &,ushort)> demandForeignIDsToDelete;
    demandForeignIDsToDelete = std::bind(&FileManager::prepareIdsForDeletion, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    groupProcessor->setCallbackFunction(demandForeignIDsToDelete);

    std::function<void(const char*,ushort)> demandDeletionByID;
    demandDeletionByID = std::bind(&FileManager::deleteEntriesByID, this, std::placeholders::_1, std::placeholders::_2);
    groupProcessor->setCallbackFunction(demandDeletionByID);

	std::function<void(char *, char*, char*, char*, std::vector<ushort> &)> demandForeignIdsToStack;
	demandForeignIdsToStack = std::bind(&FileManager::prepareForeignIdsToStack, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
	groupProcessor->setCallbackFunction(demandForeignIdsToStack);
    ///

	albumProcessor = new StructProcessor(StructType::ALBUM_STRUCT);

    std::function<void(ushort &, char*, const char*)> demandForeignID_2;
    demandForeignID_2 = std::bind(&FileManager::receiveDataFromProcessor, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    albumProcessor->setCallbackFunction(demandForeignID_2);

    std::function<void(const char*,ushort &,ushort)> demandForeignIDsToDelete_2;
    demandForeignIDsToDelete_2 = std::bind(&FileManager::prepareIdsForDeletion, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    albumProcessor->setCallbackFunction(demandForeignIDsToDelete_2);

    std::function<void(const char*,ushort)> demandDeletionByID_2;
    demandDeletionByID_2 = std::bind(&FileManager::deleteEntriesByID, this, std::placeholders::_1, std::placeholders::_2);
    albumProcessor->setCallbackFunction(demandDeletionByID_2);

	std::function<void(char *, char*, char*, char*, std::vector<ushort> &)> demandForeignIdsToStack_2;
	demandForeignIdsToStack_2 = std::bind(&FileManager::prepareForeignIdsToStack, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
	albumProcessor->setCallbackFunction(demandForeignIdsToStack_2);

    ///

	songProcessor = new StructProcessor(StructType::SONG_STRUCT);

    std::function<void(ushort &, char*,const char*)> demandForeignID_3;
    demandForeignID_3 = std::bind(&FileManager::receiveDataFromProcessor, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    songProcessor->setCallbackFunction(demandForeignID_3);

    std::function<void(const char*,ushort &,ushort)> demandForeignIDsToDelete_3;
    demandForeignIDsToDelete_3 =std::bind(&FileManager::prepareIdsForDeletion, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    songProcessor->setCallbackFunction(demandForeignIDsToDelete_3);

    std::function<void(const char*,ushort)> demandDeletionByID_3;
    demandDeletionByID_3 = std::bind(&FileManager::deleteEntriesByID, this, std::placeholders::_1, std::placeholders::_2);
    songProcessor->setCallbackFunction(demandDeletionByID_3);

	std::function<void(char *, char*, char*, char*, std::vector<ushort> &)> demandForeignIdsToStack_3;
	demandForeignIdsToStack_3 = std::bind(&FileManager::prepareForeignIdsToStack, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
	songProcessor->setCallbackFunction(demandForeignIdsToStack_3);
}

FileManager::~FileManager()
{
}

void FileManager::setDataToProcess(int argc, char **argv)
{
    if(!checkData(argc,argv)){
        std::cout << "FileManager::setDataToProcess " << Color::FG_RED << "error received" << Color::RESET << "\n\tunable to process data - emty data" << std::endl;
        return;
    }
    int counter = 0;
    char ** dataToProcess;
    if(argc<=0){
        std::cout << Color::FG_RED << "ERROR" << Color::RESET << "something happened -  argument count = 0!" << std::endl;
        return;
    }
    if(!strcmp(argv[counter],"insert")){
        counter++;
		int argcToProcess = 0;
		int start = counter+1;

		dataToProcess = (char**)malloc((argc - 2) * sizeof(char *));
		for (int var = start; var < argc; ++var) {
			dataToProcess[argcToProcess] = (char*)malloc(strlen(argv[var]) + 1);
			memcpy(dataToProcess[argcToProcess], argv[var], strlen(argv[var]) + 1);
			argcToProcess++;
		}
        if((counter<argc)&&(!strcmp(argv[counter],"--group"))){
            groupProcessor->addEntry(argcToProcess,dataToProcess);
        }
        else if ((counter<argc)&&(!strcmp(argv[counter], "--album"))) {
            albumProcessor->addEntry(argcToProcess, dataToProcess);
		}
        else if ((counter<argc)&&(!strcmp(argv[counter], "--song"))) {
            songProcessor->addEntry(argcToProcess, dataToProcess);
		}
    }
    else if ( !strcmp(argv[counter],"print"))     ///just prints information from table or memory. Unable to receive flags
    {
        counter++;
        if (!strcmp(argv[counter], "--file")||(!strcmp(argv[counter], "--memory")))
		{
			counter++;
			if (!strcmp(argv[counter], "--group")) {
                groupProcessor->printStructEntriesFromFileOrMemory(argv[counter-1]);
			}
			else if (!strcmp(argv[counter], "--album")) {
                albumProcessor->printStructEntriesFromFileOrMemory(argv[counter-1]);
			}
			else if (!strcmp(argv[counter], "--song")) {
                songProcessor->printStructEntriesFromFileOrMemory(argv[counter-1]);
			}
			else if (!strcmp(argv[counter], "--ALL")) {
                groupProcessor->printStructEntriesFromFileOrMemory(argv[counter-1]);
                albumProcessor->printStructEntriesFromFileOrMemory(argv[counter-1]);
                songProcessor->printStructEntriesFromFileOrMemory(argv[counter-1]);
			}
		}
    }
    else if ( !strcmp(argv[counter],"load")){
        counter++;
        if (!strcmp(argv[counter], "--group")) {
            groupProcessor->loadDataInMemory();
        }
        else if (!strcmp(argv[counter], "--album")) {
            albumProcessor->loadDataInMemory();
        }
        else if (!strcmp(argv[counter], "--song")) {
            songProcessor->loadDataInMemory();
        }
        else if (!strcmp(argv[counter], "--ALL")) {
            groupProcessor->loadDataInMemory();
            albumProcessor->loadDataInMemory();
            songProcessor->loadDataInMemory();
        }
    }
    else if ( !strcmp(argv[counter],"show")){      /// shows information only from memory. Can process flags
        counter++;
        char *what      = nullptr;
        char *container = nullptr;
        char *condition = nullptr;
        char *compare   = nullptr;
        for (int var = counter; var<argc;var++)
        {
            if(!strcmp(argv[var],"-what")&&(var+1<argc)){
                what = argv[var+1];
            }
            else if(!strcmp(argv[var],"-container")&&(var+1<argc)){
                container = argv[var+1];
            }
            else if(!strcmp(argv[var],"-condition")&&(var+1<argc)){
                condition = argv[var+1];
            }
            else if(!strcmp(argv[var],"-compare")&&(var+1<argc)){
                compare = argv[var+1];
            }
        }
        if((counter<argc)&&(!strcmp(argv[counter],"--group"))){
            groupProcessor->showValuesByCondition(what,condition,compare,container);
        }
        else if ((counter<argc)&&(!strcmp(argv[counter], "--album"))) {
            albumProcessor->showValuesByCondition(what,condition,compare,container);
        }
        else if ((counter<argc)&&(!strcmp(argv[counter], "--song"))) {
            songProcessor->showValuesByCondition(what,condition,compare,container);
        }
    }
    else if ( !strcmp(argv[counter],"delete")){
        counter++;
        int type = 0;
        char *where = NULL;
        char *what = NULL;
        char *container = NULL;
        bool all = false;
        DeleteOption opt = NONE;
        for (int var = counter; var < argc; ++var) {
            if (!strcmp(argv[var], "--group")) {
                type = 1;
            }
            else if (!strcmp(argv[var], "--album")) {
                type = 2;
            }
            else if (!strcmp(argv[var], "--song")) {
                type = 3;
            }
            else if (!strcmp(argv[var], "--ALL")) {
                type = 4;
            }
            else if (!strcmp(argv[var], "-column")&&((var+1)<argc)) {
                where = argv[var+1];
            }
            else if (!strcmp(argv[var], "-what")&&((var+1)<argc)) {
                what = argv[var+1];
            }
            else if (!strcmp(argv[var], "-all")&&((var+1)<argc)) {
                all = atoi(argv[var+1]);
            }
            else if (!strcmp(argv[var], "-delOpt")&&((var+1)<argc)) {
                if(atoi(argv[var+1])==0)
                   opt = NONE;
                else if (atoi(argv[var+1])==1)
                    opt = DELETE_SOFT;
                else if (atoi(argv[var+1])==2)
                    opt = DELETE_HARD;
            }
            else if (!strcmp(argv[var], "-container")&&((var+1)<argc)) {
                container =argv[var+1];
            }
        }
        switch (type) {
        case 1:
            groupProcessor->deleteData(where,what,container,all,opt);
            break;
        case 2:
            albumProcessor->deleteData(where,what,container,all,opt);
            break;
        case 3:
            songProcessor->deleteData(where,what,container,all,opt);
            break;
        case 4:
            std::cout << "currently unsupported" << std::endl;
            return;
            groupProcessor->deleteData(where,what,container,all,opt);
            albumProcessor->deleteData(where,what,container,all,opt);
            songProcessor->deleteData(where,what,container,all,opt);
            break;
        default:
            std::cout << "WRONG table name (<--group>, <--album> ,<--song>)" << std::endl;
            break;
        }

    }
    else if ( !strcmp(argv[counter],"sort")){
        counter++;
        int type = 0;
        char *what = nullptr;
        char *where = nullptr;
        bool descending = true;
        for (int var = counter; var < argc; ++var) {
            if (!strcmp(argv[var], "--group")) {
                type = 1;
            }
            else if (!strcmp(argv[var], "--album")) {
                type = 2;
            }
            else if (!strcmp(argv[var], "--song")) {
                type = 3;
            }
            else if (!strcmp(argv[var],"-what")&&((var+1)<argc)){
                what = argv[var+1];
            }
            else if (!strcmp(argv[var],"-where")&&((var+1)<argc)){
                where = argv[var+1];
            }
            else if (!strcmp(argv[var],"-desc")&&((var+1)<argc)){
                descending = atoi(argv[var+1]);
            }
        }
        switch (type) {
        case 1:
            if((what!=nullptr)&&(where!=nullptr))
                groupProcessor->sortValues(what,where,descending);
            break;
        case 2:
            if((what!=nullptr)&&(where!=nullptr))
                albumProcessor->sortValues(what,where,descending);
            break;
        case 3:
            if((what!=nullptr)&&(where!=nullptr))
                songProcessor->sortValues(what,where,descending);
            break;
        default:
            std::cout << "WRONG table name (<--group>, <--album> ,<--song>)" << std::endl;
            break;
        }
    }
    else if ( !strcmp(argv[counter],"edit")){
        counter++;
        int argcToProcess = 0;
        int start = counter+1;

        dataToProcess = (char**)malloc((argc - 2) * sizeof(char *));
        for (int var = start; var < argc; ++var) {
            dataToProcess[argcToProcess] = (char*)malloc(strlen(argv[var]) + 1);
            memcpy(dataToProcess[argcToProcess], argv[var], strlen(argv[var]) + 1);
            argcToProcess++;
        }
        if((counter<argc)&&(!strcmp(argv[counter],"--group"))){
            groupProcessor->editEntry(argcToProcess,dataToProcess);
        }
        else if ((counter<argc)&&(!strcmp(argv[counter], "--album"))) {
            albumProcessor->editEntry(argcToProcess, dataToProcess);
        }
        else if ((counter<argc)&&(!strcmp(argv[counter], "--song"))) {
            songProcessor->editEntry(argcToProcess, dataToProcess);
        }
    }
    else if ( !strcmp(argv[counter],"help")){
        help.printHelp();
    }
}

bool FileManager::checkData(int argc, char **)
{
    return true;
}

void FileManager::receiveDataFromProcessor(ushort& idToInsert, char* name, const char* tableName)
{
	std::cout << "FileManager::receiveDataFromProcessor " << tableName << std::endl;
	if(!strcmp(tableName,"GROUP_STRUCT")){
		groupProcessor->searchForForeignIDfromReceivedName(name,idToInsert);
	}
	else if (!strcmp(tableName, "ALBUM_STRUCT")){
		groupProcessor->searchForForeignIDfromReceivedName(name, idToInsert);
	}
	else if (!strcmp(tableName, "SONG_STRUCT")){
		albumProcessor->searchForForeignIDfromReceivedName(name, idToInsert);
	}
	else{
		std::cout << "UNKNOWN table name received " << tableName;
		return;
	}
    if(name!=NULL&&tableName!=NULL)
        std::cout <<"FileManager::receiveDataFromProcessor "<< name << " " << tableName << std::endl;
    return;
}

void FileManager::prepareIdsForDeletion(const char* tableName, ushort &ids, ushort id)
{
    std::cout << "FileManager::prepareIdsForDeletion" << std::endl;
    if(!strcmp(tableName,"GROUP_STRUCT")){
        albumProcessor->getIDsCount(ids,id);
    }
    else if (!strcmp(tableName, "ALBUM_STRUCT")){
        songProcessor->getIDsCount(ids,id);
    }
    else{
        std::cout << "UNKNOWN table name received " << tableName;
        return;
    }
    if(tableName!=NULL)
        std::cout <<"FileManager::prepareIdsForDeletion --> done" << tableName << std::endl;
    return;
}

void FileManager::deleteEntriesByID(const char *tableName, ushort id)
{
    std::cout << "FileManager::prepareIdsForDeletion" << std::endl;
    if(!strcmp(tableName,"GROUP_STRUCT")){
        albumProcessor->deleteByID(id);
    }
    else if (!strcmp(tableName, "ALBUM_STRUCT")){
        songProcessor->deleteByID(id);
    }
    else{
        std::cout << "UNKNOWN table name received " << tableName;
        return;
    }
    if(tableName!=NULL)
        std::cout <<"FileManager::prepareIdsForDeletion " << tableName << std::endl;
    return;
}

void FileManager::prepareForeignIdsToStack(char* container, char *what, char *condition, char*compare, std::vector<ushort>&vector)
{
	std::cout << "FileManager::prepareForeignIdsToStack" << std::endl;
	char *cont = nullptr;
	if(!strcmp(container,"group")){
		groupProcessor->showValuesByCondition(what, condition, compare, cont, &vector);
	}
	else if (!strcmp(container, "album")) {
		albumProcessor->showValuesByCondition(what, condition, compare, cont, &vector);
	}
}
