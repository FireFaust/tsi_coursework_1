#ifndef CONSOLEREADER_H
#define CONSOLEREADER_H

#include "consoleinterface.h"
#include "terminalopt.h"

#include <thread>

class ConsoleReader : public ConsoleInterface
{
public:
    ConsoleReader();
    ConsoleReader(int argc, char *argv[]);
	virtual ~ConsoleReader();
    void startReading() override;
    void stopReading() override;
    inline void setCallbackFunction(std::function<void(std::string)> func) override
    {dispatcherCallbackPtr = func;}
private:
    void giveCommand(std::string command) override;
    void init() override;
    void printInfo() override;
    void printOptions() override;

    static void *infiniteReader();
    static ConsoleReader *instance;

    std::string command;
    std::thread terminalThread;

    TerminalOptions *options;

    /**
     * void (*dispatcherCallback)(std::string)
     * pointer to dispatcher function
     */
	std::function<void(std::string)> dispatcherCallbackPtr;
};

#endif // CONSOLEREADER_H
