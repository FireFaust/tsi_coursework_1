#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <iostream>
#include <thread>

#include  "consoleinterface.h"
#include  "consolereader.h"
#include  "commandparser.h"
#include  "filemanager.h"

class Dispatcher
{
public:
    Dispatcher();
    Dispatcher(int argc, char *argv[]);
    ~Dispatcher();
    void start();
private:
    ConsoleInterface *terminal;
	CommandParser *commandParser;
	FileManager *fileManager;
    void receiveStringFromTerminal(std::string str) const;
	void receiveParsedValuesFromParser(int argc, char**) const;
	void init();
};

#endif // DISPATCHER_H
