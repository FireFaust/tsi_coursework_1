#include "dispatcher.h"

#include <functional>

Dispatcher::Dispatcher()
{
    terminal = new ConsoleReader();
	init();
}

Dispatcher::Dispatcher(int argc, char *argv[])
{
    terminal = new ConsoleReader(argc, argv);
	init();
}
Dispatcher::~Dispatcher()
{
	delete terminal;
	delete commandParser;
}

void Dispatcher::start()
{
	std::function<void(std::string)> f;
	f = std::bind(&Dispatcher::receiveStringFromTerminal, this, std::placeholders::_1);
	//void(*f)(std::string) = &receiveStringFromTerminal;
	terminal->setCallbackFunction(f);
	terminal->startReading();
}

void Dispatcher::receiveStringFromTerminal(std::string str) const
{
    commandParser->processString(str);
}

void Dispatcher::receiveParsedValuesFromParser(int argc,char **commands) const
{
    fileManager->setDataToProcess(argc,commands);
}

void Dispatcher::init()
{
	commandParser = new CommandParser();
	std::function<void(int, char**)> func;
	func = std::bind(&Dispatcher::receiveParsedValuesFromParser, this, std::placeholders::_1, std::placeholders::_2);
	commandParser->setCallbackFunction(func);
	fileManager = new FileManager();
}
