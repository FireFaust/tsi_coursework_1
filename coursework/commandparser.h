#ifndef COMMANDPARSER_H
#define COMMANDPARSER_H

#include "ostreamcolor/modifier.h"

#include <functional>

class CommandParser
{
public:
	CommandParser();
	~CommandParser();
	void processString(std::string command);
	inline void setCallbackFunction(std::function<void(int,char**)>  func) { dispatcherCallbackPtr = func; }
private:
	bool isQuotesCountEven(const std::string);
	char **commands;
	char *singleCommand;
	std::function<void(int,char**)> dispatcherCallbackPtr;
    unsigned int commandsCount = 0;
    void insertNewCommandInList(char ** tCommands,unsigned  int &count);
};

#endif //COMMANDPARSER_H
