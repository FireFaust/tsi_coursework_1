
#include  "structprocessor.h"
#include  "ostreamcolor/modifier.h"

#include <iostream>
#include <cstring>

#include <typeinfo>

#include <sstream>

#define system_cout std::cout << Color::FG_YELLOW
#define critical_cout std::cout << Color::FG_RED

//insert --group -name "test test2" -genre rock -country latvia -year 1997
//insert --album -name "TAAA" -count 10 -year 2000 -group "test test2"
//insert --song -name "song" -duration 10 -album "TAAA"

//delete --song -name name -what song -all 1 -delOpt 0 -container "TAAA"

#include <string>

#if defined(_WIN64) || defined(_WIN32)
#define strcpy strcpy_s
#endif

#define MainStruct(X) static_cast<struct X*>(mainStruct)
#define _UseStruct(X,Y) static_cast<struct X*>(Y)

/// totally not tested
//#define _(x) std::cout << #x << " " << x << std::endl

void convert(int number)
{
   std::ostringstream sin;
   sin << number;
   std::string val = sin.str();

}

StructProcessor::StructProcessor(StructType type) : type(type)
{
	_processorType = NULL;
    _array = nullptr;
    _arraySize = 0;
    std::cout << "StructProcessor <"<<Color::FG_RED << getProcessorType() <<Color::RESET << ">" << Color::FG_YELLOW << " INITIALISED" <<Color::RESET<< std::endl;
    initFile();
    initStruct(mainStruct);

}

const char* StructProcessor::getProcessorType()
{
	switch (type)
	{
	case StructType::BASE_STUCT:
		if(_processorType==NULL){
			_processorType = "BASE_STRUCT";
		}
		return _processorType;
		break;
	case StructType::ALBUM_STRUCT:
        if(_processorType==NULL){
            _processorType = "ALBUM_STRUCT";
        }
        return _processorType;
		break;
    case StructType::GROUP_STRUCT:
        if(_processorType==NULL){
            _processorType = "GROUP_STRUCT";
        }
        return _processorType;
		break;
	case StructType::SONG_STRUCT:
        if(_processorType==NULL){
            _processorType = "SONG_STRUCT";
        }
        return _processorType;
		break;
	default:
		return NULL;
    }
}

void StructProcessor::addEntry(int argc, char **argv)   ///DEAL WITH ERROR
{
	if (!ebnFile.is_open())
	{
		system_cout << getProcessorType();
		critical_cout << " file not opened\n" << Color::RESET;
		for (int i = 0; i< argc; i++) {
			free(argv[i]);
		}
		free(argv);
		return;
	}
    std::cout << "StructProcessor::addEntry " << std::endl;

	switch (type)
	{
	case SONG_STRUCT:
		MainStruct(SongStruct)->_foreignID = 0;
		break;
	case ALBUM_STRUCT:
		MainStruct(AlbumStruct)->_foreignID = 0;
		break;
	default:
		break;
	}

    for(int i=0; i<argc;i++){
		if (!strcmp(argv[i], "-name"))
			strcpy(mainStruct->_name, argv[i + 1]);
	    switch (type)
	    {
        case GROUP_STRUCT:
			if (!strcmp(argv[i], "-country") && ((i+1)<argc))
				strcpy(MainStruct(GroupStruct)->_country, argv[i + 1]);
			else if (!strcmp(argv[i], "-genre") && ((i + 1)<argc))
				strcpy(MainStruct(GroupStruct)->_genre, argv[i + 1]);
			else if (!strcmp(argv[i], "-year") && ((i + 1)<argc))
				MainStruct(GroupStruct)->_year = atoi(argv[i + 1]);
			break;
		case ALBUM_STRUCT:
 			if (!strcmp(argv[i], "-count") && ((i + 1)<argc)) {
				MainStruct(AlbumStruct)->_count = atoi(argv[i + 1]);
			}
			else if (!strcmp(argv[i], "-year") && ((i + 1)<argc)) {
				MainStruct(AlbumStruct)->_year = atoi(argv[i + 1]);
			}
			else if (!strcmp(argv[i], "-group") && ((i + 1)<argc)) {
				fileManagerCallbackPtr_demandForeignID(MainStruct(AlbumStruct)->_foreignID, argv[i + 1], getProcessorType());
			}
			break;
		case SONG_STRUCT:
			if (!strcmp(argv[i], "-duration") && ((i + 1)<argc)) {
				MainStruct(SongStruct)->_duration = atoi(argv[i + 1]);
			}
			else if (!strcmp(argv[i], "-album") && ((i + 1)<argc)) {
				fileManagerCallbackPtr_demandForeignID(MainStruct(SongStruct)->_foreignID, argv[i + 1], getProcessorType());
			}
			break;
        default:
            break;
	    }
    }
	for(int i=0; i< argc;i++){
		free(argv[i]);
	}
    free(argv);


	switch (type)
	{
	case ALBUM_STRUCT:
		if(!MainStruct(AlbumStruct)->_foreignID)
		{
			std::cout << "UNABLE TO GET ID FROM PROVIDED GROUP NAME <-group> is correct?" << std::endl;
			return;
		}
		break;
	case SONG_STRUCT:
		if (!MainStruct(SongStruct)->_foreignID)
		{
			std::cout << "UNABLE TO GET ID FROM PROVIDED ALBUM NAME <-album> is correct?" << std::endl;
			return;
		}
		break;
	default:
		break;
	}

    getMaxID(MainStruct(BaseStruct)->ID);


    if(isNameUnical(mainStruct))
        return;

    ebnFile.clear();  ///necessary to reach  0 pos after eof
    ebnFile.seekg(0, std::ios::end);


	switch (type)
	{
    case GROUP_STRUCT:
		ebnFile.write(reinterpret_cast<char*>(&*MainStruct(GroupStruct)), getStructSize());
		break;
	case ALBUM_STRUCT:
		ebnFile.write(reinterpret_cast<char*>(&*MainStruct(AlbumStruct)), getStructSize());
		break;
	case SONG_STRUCT:
		ebnFile.write(reinterpret_cast<char*>(&*MainStruct(SongStruct)), getStructSize());
		break;
	default:
        std::cout << "UNABLE TO WRITE DATA TO FILE << SOMETHING WRONG WITH STRUCT TYPE" << std::endl;
		break;
	}
}

void StructProcessor::initFile()
{
    fileName = getProcessorType();
    fileName.append(".ebn");
    ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::binary | std::ios::app);
    if (!ebnFile.is_open())   ///in case if file is no existed
    {
        ebnFile.close();
        ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
    }
}

void StructProcessor::initStruct(BaseStruct *&_struct)
{
    switch (type) {
    case StructType::GROUP_STRUCT:
        _struct = new GroupStruct;
        break;
    case StructType::ALBUM_STRUCT:
        _struct = new AlbumStruct;
        break;
    case StructType::SONG_STRUCT:
        _struct = new SongStruct;
        break;
    default:
        /// necessary to throw error
        critical_cout << "UNABLE TO INIT STRUCTURE" << Color::RESET << std::endl;
        break;
    }
}

std::size_t StructProcessor::getStructSize()
{
    switch (type) {
    case StructType::GROUP_STRUCT:
        return sizeof(GroupStruct);
        break;
    case StructType::ALBUM_STRUCT:
        return sizeof(AlbumStruct);
        break;
    case StructType::SONG_STRUCT:
        return sizeof(SongStruct);
        break;
    default:
        /// necessary to throw error
        critical_cout << "UNABLE TO INIT STRUCTURE" << Color::RESET << std::endl;
        return -1;
        break;
    }
}

void StructProcessor::getMaxID(ushort &id)
{

	if (!ebnFile.is_open())
	{
		system_cout << getProcessorType();
		critical_cout << " file not opened\n" << Color::RESET;
		id = 0;
		return;
	}

    BaseStruct *bSt;
    initStruct(bSt);
    id = 0;

	ebnFile.clear();  ///necessary to reach  0 pos after eof
	ebnFile.seekg(std::ios::beg);

    while (ebnFile.read(reinterpret_cast<char*>(&*bSt),  getStructSize() ) )
    {
        if (id < (*bSt).ID)
            id =  (*bSt).ID;
    }
    id++;
    delete bSt;
}

bool StructProcessor::isNameUnical(BaseStruct *_struct)
{
	bool result = true;
	if (!ebnFile.is_open())
	{
		system_cout << getProcessorType();
		critical_cout << " file not opened\n" << Color::RESET;
		return !result;
	}

	ebnFile.clear();  ///necessary to reach  0 pos after eof
	ebnFile.seekg(std::ios::beg);


	BaseStruct *bSt;
	initStruct(bSt);

	ebnFile.clear();  ///necessary to reach  0 pos after eof
	ebnFile.seekg(std::ios::beg);

	if (type == SONG_STRUCT){
		while (ebnFile.read(reinterpret_cast<char*>(&*bSt), getStructSize()))
		{
			if ((!strcmp(_UseStruct(SongStruct,bSt)->_name, _struct->_name))
				&&(_UseStruct(SongStruct,_struct)->_foreignID)==_UseStruct(SongStruct,bSt)->_foreignID ) {
				std::cout << "StructProcessor::isNameUnical: " << "name unicality - " << Color::FG_RED << "FALSE" << Color::RESET << std::endl;
				result = false;
				break;
			}
		}
	}
	else{
		while (ebnFile.read(reinterpret_cast<char*>(&*bSt), getStructSize()))
		{
			if (!strcmp(bSt->_name, _struct->_name)) {
				std::cout << "StructProcessor::isNameUnical: " << "name unicality - " << Color::FG_RED << "FALSE" << Color::RESET << std::endl;
				result = false;
				break;
			}
		}
	}

    delete bSt;
    return !result;
}

void StructProcessor::sortValues(char *what, char *where, bool descending)
{
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }

    loadDataInMemory();

    int _what = 0;
    int _where = 0;

    if(!strcmp(what,"string"))
        _what = 1;
    else if(!strcmp(what,"int"))
        _what = 2;

    if(!strcmp(where,"name"))
        _where = 1;
    else if(!strcmp(where,"country"))
        _where = 2;
    else if(!strcmp(where,"year"))
        _where = 3;
    else if(!strcmp(where,"genre"))
        _where = 4;
    else if(!strcmp(where,"count"))
        _where = 5;
    else if(!strcmp(where,"duration"))
        _where = 6;


    if(!what||!where){
        std::cout << "StructProcessor::sortValues " << "wrong <-what> or <-where>" << std::endl;
        return;
    }

    for (int var = 1; var < _arraySize; ++var) {
        for (int varJ = 1; varJ < _arraySize; ++varJ) {
            if(_what==1&&_where==1)
            {
                switch (type) {
                case GROUP_STRUCT:
                    if(!descending){
                        if (strcmp(_UseStruct(GroupStruct, _array)[varJ-1]._name, _UseStruct(GroupStruct, _array)[varJ]._name) > 0) {
                            std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                        }
                    }
                    else
                        if (strcmp(_UseStruct(GroupStruct, _array)[varJ-1]._name,_UseStruct(GroupStruct, _array)[varJ]._name) < 0) {
                            std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                        }
                    break;
                case ALBUM_STRUCT:
                    if(!descending){
                        if (strcmp(_UseStruct(AlbumStruct, _array)[varJ-1]._name, _UseStruct(AlbumStruct, _array)[varJ]._name) > 0) {
                            std::swap(_UseStruct(AlbumStruct, _array)[varJ-1],_UseStruct(AlbumStruct, _array)[varJ]);
                        }
                    }
                    else
                        if (strcmp(_UseStruct(AlbumStruct, _array)[varJ-1]._name,_UseStruct(AlbumStruct, _array)[varJ]._name) < 0) {
                            std::swap(_UseStruct(AlbumStruct, _array)[varJ-1],_UseStruct(AlbumStruct, _array)[varJ]);
                        }
                    break;
                case SONG_STRUCT:
                    if(!descending){
                        if (strcmp(_UseStruct(SongStruct, _array)[varJ-1]._name, _UseStruct(SongStruct, _array)[varJ]._name) > 0) {
                            std::swap(_UseStruct(SongStruct, _array)[varJ-1],_UseStruct(SongStruct, _array)[varJ]);
                        }
                    }
                    else
                        if (strcmp(_UseStruct(SongStruct, _array)[varJ-1]._name,_UseStruct(SongStruct, _array)[varJ]._name) < 0) {
                            std::swap(_UseStruct(SongStruct, _array)[varJ-1],_UseStruct(SongStruct, _array)[varJ]);
                        }
                    break;
                default:
                    break;
                }
            }
            else if(_what==1&&_where==2){
                if(!descending){
                    if (strcmp(_UseStruct(GroupStruct, _array)[varJ-1]._country, _UseStruct(GroupStruct, _array)[varJ]._country) > 0) {
                        std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                    }
                }
                else
                    if (strcmp(_UseStruct(GroupStruct, _array)[varJ-1]._country,_UseStruct(GroupStruct, _array)[varJ]._country) < 0) {
                        std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                    }
            }
            else if(_what==2&&_where==3){
                switch (type) {
                case GROUP_STRUCT:
                    if(!descending){
                        if (_UseStruct(GroupStruct, _array)[varJ-1]._year > _UseStruct(GroupStruct, _array)[varJ]._year) {
                            std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                        }
                    }
                    else
                        if (_UseStruct(GroupStruct, _array)[varJ-1]._year < _UseStruct(GroupStruct, _array)[varJ]._year) {
                            std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                        }
                    break;
                case ALBUM_STRUCT:
                    if(!descending){
                        if (_UseStruct(AlbumStruct, _array)[varJ-1]._year > _UseStruct(AlbumStruct, _array)[varJ]._year) {
                            std::swap(_UseStruct(AlbumStruct, _array)[varJ-1],_UseStruct(AlbumStruct, _array)[varJ]);
                        }
                    }
                    else
                        if  (_UseStruct(AlbumStruct, _array)[varJ-1]._year < _UseStruct(AlbumStruct, _array)[varJ]._year) {
                            std::swap(_UseStruct(AlbumStruct, _array)[varJ-1],_UseStruct(AlbumStruct, _array)[varJ]);
                        }
                    break;
                default:
                    break;
                }
            }
            else if(_what==1&&_where==4){
                if(!descending){
                    if (strcmp(_UseStruct(GroupStruct, _array)[varJ-1]._genre, _UseStruct(GroupStruct, _array)[varJ]._genre) > 0) {
                        std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                    }
                }
                else
                    if (strcmp(_UseStruct(GroupStruct, _array)[varJ-1]._genre,_UseStruct(GroupStruct, _array)[varJ]._genre) < 0) {
                        std::swap(_UseStruct(GroupStruct, _array)[varJ-1],_UseStruct(GroupStruct, _array)[varJ]);
                    }
            }
            else if(_what==2&&_where==5){
                if(!descending){
                    if (_UseStruct(AlbumStruct, _array)[varJ-1]._count > _UseStruct(AlbumStruct, _array)[varJ]._count) {
                        std::swap(_UseStruct(AlbumStruct, _array)[varJ-1],_UseStruct(AlbumStruct, _array)[varJ]);
                    }
                }
                else
                    if  (_UseStruct(AlbumStruct, _array)[varJ-1]._count < _UseStruct(AlbumStruct, _array)[varJ]._count) {
                        std::swap(_UseStruct(AlbumStruct, _array)[varJ-1],_UseStruct(AlbumStruct, _array)[varJ]);
                    }
            }
            else if(_what==2&&_where==6){
                if(!descending){
                    if (_UseStruct(SongStruct, _array)[varJ-1]._duration > _UseStruct(SongStruct, _array)[varJ]._duration) {
                        std::swap(_UseStruct(SongStruct, _array)[varJ-1],_UseStruct(SongStruct, _array)[varJ]);
                    }
                }
                else
                    if  (_UseStruct(SongStruct, _array)[varJ-1]._duration < _UseStruct(SongStruct, _array)[varJ]._duration) {
                        std::swap(_UseStruct(SongStruct, _array)[varJ-1],_UseStruct(SongStruct, _array)[varJ]);
                    }
            }
        }
    }

    ebnFile.close();
    ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);

    switch(type){
    case GROUP_STRUCT:
        for(int i=0; i< _arraySize;i++)
            ebnFile.write(reinterpret_cast<char*>(&_UseStruct(GroupStruct, _array)[i]), getStructSize());
        break;
    case ALBUM_STRUCT:
        for(int i=0; i< _arraySize;i++)
            ebnFile.write(reinterpret_cast<char*>(&_UseStruct(AlbumStruct, _array)[i]), getStructSize());
        break;
    case SONG_STRUCT:
        for(int i=0; i< _arraySize;i++)
            ebnFile.write(reinterpret_cast<char*>(&_UseStruct(SongStruct, _array)[i]), getStructSize());
        break;
    }

    std::cout << "StructProcessor::sortValues" << "done" << std::endl;
}

void StructProcessor::showValuesByCondition(char *what, char *condition, char *compare, char *container, std::vector<ushort> *vector)
{
    std::cout << "StructProcessor::showValuesByCondition " <<getProcessorType() << std::endl;

    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }

    if(container==nullptr&&vector==NULL)
    {
        switch (type) {
        case ALBUM_STRUCT:
            if(!strcmp(what,"name") || !strcmp(what,"count")  || !strcmp(what,"year" ) ){
                std::cout << "UNABLE TO SHOW BY" << Color::FG_CYAN << " <-what> " <<Color::RESET<< ",also no container provided" << std::endl;
                return;
            }
            break;
        case SONG_STRUCT:
            if(!strcmp(what,"name") || !strcmp(what,"duration") ){
                std::cout << "UNABLE TO SHOW BY" << Color::FG_CYAN << " <-what> " <<Color::RESET<< ",also no container provided" << std::endl;
                return;
            }
            break;
        default:
            break;
        }
    }

    if((!strcmp(what,"name") && !strcmp(what,"country")  && !strcmp(what,"genre" ))&&!strcmp(condition,"=")){
        std::cout << "with " << Color::FG_CYAN << "<name>, <genre>, <country>" << Color::RESET << " only < = > condition allowed" << std::endl;
        return;
    }

    int _what = 0;
    int _condition = -1;

	if (!strcmp(what, "name"))
		_what = 1;
	else if (!strcmp(what, "country"))
		_what = 2;
	else if (!strcmp(what, "genre"))
		_what = 3;
	else if (!strcmp(what, "count"))
		_what = 4;
	else if (!strcmp(what, "year"))
		_what = 5;
	else if (!strcmp(what, "duration"))
		_what = 6;
	else if (!strcmp(what, "foreign") && vector != NULL)
		_what = 7;

    if(!strcmp(condition,"!="))
        _condition = 0;
    else if(!strcmp(condition,"="))
        _condition = 1;
    else if(!strcmp(condition,">"))
        _condition = 2;
    else if(!strcmp(condition,"<"))
        _condition = 3;
    else if(!strcmp(condition,">="))
        _condition = 4;
    else if(!strcmp(condition,"<="))
        _condition = 5;

    BaseStruct *bSt;
    initStruct(bSt);

    //show --group -what genre -condition = -compare "rock"
    //show --group -what country -condition = -compare "latvia"
    //show --group -what year -condition > -compare 2000

	//show --album -what year -condition > -compare 1980 -container group
	//show --song -what year -condition > -compare 1980 -container group

    ebnFile.clear();  ///necessary to reach  0 pos after eof
    ebnFile.seekg(std::ios::beg);

    if(container==nullptr){  ///simple search (single table)
        switch (type) {
        case GROUP_STRUCT:
            while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(GroupStruct, bSt)), getStructSize())){
                switch (_what) {
                case 1:
                    if (!strcmp(_UseStruct(GroupStruct, bSt)->_name, compare)){
                        if (vector != 0 && vector != NULL&&vector != nullptr) vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                    break;
                case 2:
                    if(!strcmp(_UseStruct(GroupStruct, bSt)->_country,compare)){
                        if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                    break;
                case 3:
                    if(!strcmp(_UseStruct(GroupStruct, bSt)->_genre,compare)){
                        if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                    break;
                case 5:
                    switch (_condition) {
                    case 0:
                        if(_UseStruct(GroupStruct, bSt)->_year!=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                        break;
                    case 1:
                        if(_UseStruct(GroupStruct, bSt)->_year==atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                        break;
                    case 2:
                        if(_UseStruct(GroupStruct, bSt)->_year>atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                        break;
                    case 3:
                        if(_UseStruct(GroupStruct, bSt)->_year<atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                        break;
                    case 4:
                        if(_UseStruct(GroupStruct, bSt)->_year>=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(GroupStruct, bSt)->ID); else std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;}
                        break;
                    case 5:
                        if(_UseStruct(GroupStruct, bSt)->_year<=atoi(compare))
                            std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;
                        break;
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
            }
            break;
        case ALBUM_STRUCT:
            while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(AlbumStruct, bSt)), getStructSize())){
                switch (_what) {
                case 1:
                    if(!strcmp(_UseStruct(AlbumStruct, bSt)->_name,compare)){
                        if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                    break;
                case 4:
                    switch (_condition) {
                    case 0:
                        if(_UseStruct(AlbumStruct, bSt)->_count!=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 1:
                        if(_UseStruct(AlbumStruct, bSt)->_count==atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 2:
                        if(_UseStruct(AlbumStruct, bSt)->_count>atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 3:
                        if(_UseStruct(AlbumStruct, bSt)->_count<atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 4:
                        if(_UseStruct(AlbumStruct, bSt)->_count>=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 5:
                        if(_UseStruct(AlbumStruct, bSt)->_count<=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    default:
                        break;
                    }
                    break;
                case 5:
                    switch (_condition) {
                    case 0:
                        if(_UseStruct(AlbumStruct, bSt)->_year!=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 1:
                        if(_UseStruct(AlbumStruct, bSt)->_year==atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 2:
                        if(_UseStruct(AlbumStruct, bSt)->_year>atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 3:
                        if(_UseStruct(AlbumStruct, bSt)->_year<atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 4:
                        if(_UseStruct(AlbumStruct, bSt)->_year>=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    case 5:
                        if(_UseStruct(AlbumStruct, bSt)->_year<=atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
                        break;
                    default:
                        break;
                    }
                    break;
				case 7: ///private
					switch (_condition) {
					case 0:
                        if (_UseStruct(AlbumStruct, bSt)->_foreignID != atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
						break;
					case 1:
                        if (_UseStruct(AlbumStruct, bSt)->_foreignID == atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
						break;
					case 2:
                        if (_UseStruct(AlbumStruct, bSt)->_foreignID>atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
						break;
					case 3:
                        if (_UseStruct(AlbumStruct, bSt)->_foreignID<atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
						break;
					case 4:
                        if (_UseStruct(AlbumStruct, bSt)->_foreignID >= atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
						break;
					case 5:
                        if (_UseStruct(AlbumStruct, bSt)->_foreignID <= atoi(compare)){
                            if (vector != 0 && vector != NULL&&vector != nullptr)vector->push_back(_UseStruct(AlbumStruct, bSt)->ID); else std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;}
						break;
					default:
						break;
					}
					break;
                default:
                    break;
                }
            }
            break;
        case SONG_STRUCT:
            switch (_what) {
            case 1:
                if(!strcmp(_UseStruct(SongStruct, bSt)->_name,compare))
                    std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
                break;
            case 6:
                switch (_condition) {
                case 0:
                    if(_UseStruct(SongStruct, bSt)->_duration!=atoi(compare))
                        std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
                    break;
                case 1:
                    if(_UseStruct(SongStruct, bSt)->_duration==atoi(compare))
                        std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
                    break;
                case 2:
                    if(_UseStruct(SongStruct, bSt)->_duration>atoi(compare))
                        std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
                    break;
                case 3:
                    if(_UseStruct(SongStruct, bSt)->_duration<atoi(compare))
                        std::cout  << *_UseStruct(GroupStruct, bSt) << std::endl;
                    break;
                case 4:
                    if(_UseStruct(SongStruct, bSt)->_duration>=atoi(compare))
                        std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
                    break;
                case 5:
                    if(_UseStruct(SongStruct, bSt)->_duration<=atoi(compare))
                        std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
                    break;
                default:
                    break;
                }
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
    }
    else{  ///Across the rainbow bridge to Valhalla Odin's waiting for me.
        switch (type) {
		case ALBUM_STRUCT: {
            std::vector<ushort> *_foreignIds = new std::vector<ushort>;
			fileManagerCallback_demandForeignIDsToContainer(container, what, condition, compare, *_foreignIds);
            std::cout << "Found " << _foreignIds->size() << " matches in albums" << std::endl;
			while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(AlbumStruct, bSt)), getStructSize())) {
				bool isOk = false;
				unsigned int i = 0;
				while (i < _foreignIds->size()) {
					if (_UseStruct(AlbumStruct, bSt)->_foreignID == _foreignIds->at(i)){
						isOk = true;
						break;
					}
					i++;
				}
				if(isOk)
                    std::cout  << *_UseStruct(AlbumStruct, bSt) << std::endl;
			}
			delete _foreignIds;
        }
            break;
		case SONG_STRUCT: {
			std::vector<ushort> *_foreignIds = new std::vector<ushort>;
			fileManagerCallback_demandForeignIDsToContainer(container, what, condition, compare, *_foreignIds);
            if(_foreignIds->size()){
                std::cout << "Found " << _foreignIds->size() << " matches in groups" << std::endl;
				if(!strcmp(container,"group")){
					char tContainer[] = "album";
					char tWhat[] = "foreign";
					char tCondition[] =  "=";					
					for(unsigned int i =0; i<_foreignIds->size();i++){
#if defined(UNIX) || defined(__unix__) || defined(LINUX) || defined(__linux__) || defined(__linux)
                        char tCompare[33];
                        std::ostringstream ostr; //output string stream
                        ostr << _foreignIds->at(i); //use the string stream just like cout,
                        std::string str = ostr.str();
                        str.copy(tCompare,str.size());
#define secondCheck_DELETE_KEY getch() == 126
#elif defined(_WIN64) || defined(_WIN32)
						char tCompare[33];
						_itoa_s(_foreignIds->at(i), tCompare,33, 10);
#define secondCheck_DELETE_KEY true
#endif
						std::vector<ushort> *_songIds = new std::vector<ushort>;
						fileManagerCallback_demandForeignIDsToContainer(tContainer, tWhat, tCondition, tCompare, *_songIds);
                        std::cout << "Found " << _songIds->size() << "matches in albums" << std::endl;
						while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(SongStruct, bSt)), getStructSize())) {
							bool isOk = false;
							unsigned int j = 0;
							while (j < _songIds->size()) {
								if (_UseStruct(SongStruct, bSt)->_foreignID == _songIds->at(j)) {
									isOk = true;
									break;
								}
								j++;
							}
							if (isOk)
                                std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
						}
						std::cout << "total album count " << _songIds->size() << std::endl;
						delete _songIds;
					}
				}
				else{
                    std::cout << "Found " << _foreignIds->size() << "matches in albums" << std::endl;
					while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(SongStruct, bSt)), getStructSize())) {
						bool isOk = false;
						unsigned int i = 0;
						while (i < _foreignIds->size()) {
							if (_UseStruct(SongStruct, bSt)->_foreignID == _foreignIds->at(i)) {
								isOk = true;
								break;
							}
							i++;
						}
						if (isOk)
                            std::cout  << *_UseStruct(SongStruct, bSt) << std::endl;
					}
				}
			}
			delete _foreignIds; 
		}
            break;
        default:
            break;
        }
    }
    delete bSt;
}

void StructProcessor::countMatches(char *what, ushort &match, ushort &id)
{
    std::cout << "count matches " << getProcessorType() << std::endl;
    match = 0;
    id = 0;
    for (unsigned short i=0; i< _arraySize;i++){
        switch (type)
        {
        case GROUP_STRUCT:  ///should be one
			if (!strcmp(_UseStruct(GroupStruct, _array)[i]._name, what)) {
				match++;
				id = _UseStruct(GroupStruct, _array)[i].ID;
			}
            break;
        case ALBUM_STRUCT:   ///should be one
            if(!strcmp(_UseStruct(AlbumStruct, _array)[i]._name,what)){
                match++;
                id = _UseStruct(AlbumStruct, _array)[i].ID;
            }
            break;
        case SONG_STRUCT:           
            if(!strcmp(_UseStruct(SongStruct, _array)[i]._name,what)){
                std::cout << "matching "<< what << " with " << _UseStruct(SongStruct, _array)[i] << std::endl;
                match++;
            }
            break;
        default:
            break;
        }
    }
    std::cout << "MATCHES COUNT "<< match << std::endl;
}

void StructProcessor::deleteByID(ushort id)
{
    std::cout << "StructProcessor::deleteByID " << getProcessorType() << std::endl;
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }
    if(id == 0)
    {
        std::cout << "StructProcessor::deleteByID " << getProcessorType() << " unable to delete " << id << std::endl;
        return;
    }
    loadDataInMemory();
	const int _size = _arraySize;
    switch (type)
    {
    case ALBUM_STRUCT:   ///should be one
	{
		std::cout << getProcessorType() << " magic" << std::endl;
		if (ebnFile.is_open())   ///in case if file is no existed
		{
			ushort ids;
			ebnFile.close();
			ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
			for (int var = 0; var < _size; ++var)
			{				
				if (_UseStruct(AlbumStruct, _array)[var]._foreignID != id)					
					ebnFile.write(reinterpret_cast<char*>(&_UseStruct(AlbumStruct, _array)[var]), getStructSize());
				else {
                    std::cout << "\tdeleting album(by id)\n" << _UseStruct(AlbumStruct, _array)[var]._name << std::endl;
					fileManagerCallbackPtr_demandForeignIDsToDelete(getProcessorType(), ids, _UseStruct(AlbumStruct,_array)[var].ID);
					//std::cout <<Color::FG_GREEN<< _UseStruct(AlbumStruct, _array)[var] << " " << ids << Color::RESET << std::endl;				
					fileManagerCallbackPtr_demandDeletionByID(getProcessorType(), _UseStruct(AlbumStruct,_array)[var].ID);
				}
			}
		}
	}
        break;
    case SONG_STRUCT:
        if (ebnFile.is_open())   ///in case if file is no existed
        {
            ebnFile.close();
            ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
            for (int var = 0; var < _arraySize; ++var)
            {
                if(_UseStruct(SongStruct, _array)[var]._foreignID!=id)
                    ebnFile.write(reinterpret_cast<char*>(&_UseStruct(SongStruct, _array)[var]), getStructSize());
                else
                    std::cout << "\tdeleting song(by ids) \n" << _UseStruct(SongStruct, _array)[var] << " " << id<< std::endl;
            }
        }
        break;
    default:
        break;
    }
}

void StructProcessor::getIDsCount(ushort &ids, ushort id)
{
    std::cout << "StructProcessor::getIDsCount" << std::endl;
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }

    loadDataInMemory();

    ids = 0;
    switch (type)
    {
    case ALBUM_STRUCT:
        for (int var = 0; var < _arraySize; ++var) {
            if(_UseStruct(AlbumStruct, _array)[var]._foreignID==id)
                ids++;
        }
        break;
    case SONG_STRUCT:
        for (int var = 0; var < _arraySize; ++var) {
            if(_UseStruct(SongStruct, _array)[var]._foreignID==id)
                ids++;
        }
        break;
    default:
        break;
    }
}

void StructProcessor::editEntry(int argc, char **argv)
{
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        for (int i = 0; i< argc; i++) {
            free(argv[i]);
        }
        free(argv);
        return;
    }

    loadDataInMemory();

    char *what = nullptr;
    char *container = nullptr;

    for(int i =0; i< argc;i++){
        if(!strcmp(argv[i],"-what")){
            if(i+1<argc)
                what = argv[i+1];
        }
        else if(!strcmp(argv[i],"-container")){
            if(i+1<argc)
                container = argv[i+1];
        }
    }

    if(what==nullptr){
        std::cout << "StructProcessor::editEntry " << "unable to edit entry -wrong " << Color::FG_CYAN << "<-what>" <<Color::RESET << std::endl;
        return;
    }

    BaseStruct *_bSt;
    initStruct(_bSt);

    _bSt->ID = 0;

    int i = 0;
    switch (type) {
    case GROUP_STRUCT:{
        while(i<_arraySize){
            if (!strcmp(_UseStruct(GroupStruct, _array)[i]._name,what)){
                _bSt = &_UseStruct(GroupStruct, _array)[i];
                break;
            }
            i++;
        }
    }
    break;
    case ALBUM_STRUCT:{
        while(i<_arraySize){
            if (!strcmp(_UseStruct(AlbumStruct, _array)[i]._name,what)){
                _bSt = &_UseStruct(AlbumStruct, _array)[i];
                break;
            }
            i++;
        }
    }
    break;
    case SONG_STRUCT:{
        if(container==nullptr){
            delete _bSt;
            std::cout << "StructProcessor::editEntry " << "unable to edit entry -wrong " << Color::FG_CYAN << "<-container>" <<Color::RESET << std::endl;
            return;
        }
        int i =0;
        ushort id;
        fileManagerCallbackPtr_demandForeignID(id, container, getProcessorType());
        if(!id){
            delete _bSt;
            std::cout << "StructProcessor::editEntry " << "unable to edit entry -wrong " << Color::FG_CYAN << "<-container>" <<Color::RESET << std::endl;
            return;
        }
        while(i<_arraySize){
            if ((!strcmp(_UseStruct(SongStruct, _array)[i]._name,what))&&(id==_UseStruct(SongStruct, _array)[i]._foreignID)){
                _bSt = &_UseStruct(SongStruct, _array)[i];
                break;
            }
            i++;
        }
    }
    break;
    default:
        std::cout << "StructProcessor::editEntry" << "unable to edit entry" << std::endl;
        delete _bSt;
        return;
    }

    if(!_bSt->ID)
    {
         std::cout << "StructProcessor::editEntry" << "unable to locate entry ==> " << what << std::endl;
         delete _bSt;
         return;
    }

    for (int var = 0; var < argc; ++var) {
        if(!strcmp(argv[var],"-name")){
            if(var+1<argc) {
                strcpy(_bSt->_name, argv[var + 1]);
            }
        }
        else {
            switch (type) {
            case GROUP_STRUCT:
                if(!strcmp(argv[var],"-country")){
                    if(var+1<argc) {
                        strcpy(_UseStruct(GroupStruct, _bSt)->_country, argv[var + 1]);
                    }
                }
                else if(!strcmp(argv[var],"-genre")){
                    if(var+1<argc) {
                        strcpy(_UseStruct(GroupStruct, _bSt)->_genre, argv[var + 1]);
                    }
                }
                else if(!strcmp(argv[var],"-year")){
                    if(var+1<argc) {
                        _UseStruct(GroupStruct, _bSt)->_year = atoi( argv[var + 1]);
                    }
                }
                break;
            case ALBUM_STRUCT:
                if(!strcmp(argv[var],"-count")){
                    if(var+1<argc) {
                        _UseStruct(AlbumStruct, _bSt)->_count = atoi(argv[var + 1]);
                    }
                }
                else if(!strcmp(argv[var],"-year")){
                    if(var+1<argc) {
                        _UseStruct(AlbumStruct, _bSt)->_year = atoi(argv[var + 1]);
                    }
                }
                break;
            case SONG_STRUCT:
                if(!strcmp(argv[var],"-duration")){
                    if(var+1<argc) {
                        _UseStruct(SongStruct, _bSt)->_duration = atoi(argv[var + 1]);
                    }
                }
                break;
            default:
                break;
            }
        }
    }

    if(isNameUnical(_bSt)&&type!=SONG_STRUCT)
    {
        std::cout << "for group and albums name should be unical" << std::endl;
        return;
    }

    ebnFile.close();
    ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);

    switch (type) {
    case GROUP_STRUCT:
        for (int var = 0; var < _arraySize; ++var) {
                ebnFile.write(reinterpret_cast<char*>(&_UseStruct(GroupStruct,_array)[var]), getStructSize());
        }
        break;
    case ALBUM_STRUCT:
        for (int var = 0; var < _arraySize; ++var) {
                ebnFile.write(reinterpret_cast<char*>(&_UseStruct(AlbumStruct,_array)[var]), getStructSize());
        }
        break;
    case SONG_STRUCT:
        for (int var = 0; var < _arraySize; ++var) {
                ebnFile.write(reinterpret_cast<char*>(&_UseStruct(AlbumStruct,_array)[var]), getStructSize());
        }
        break;
    default:
        break;
    }


    std::cout << "StructProcessor::editEntry " << getProcessorType()  << " done" << std::endl;
}

void StructProcessor::loadDataInMemory()
{
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }

    std::cout << "loading data in memory " << getProcessorType() << std::endl;

    ebnFile.clear();  ///necessary to reach  0 pos after eof
    ebnFile.seekg(std::ios::beg);

    ebnFile.seekg(0, std::ios::end);
    _arraySize = ebnFile.tellg();
    _arraySize /= getStructSize();
    ebnFile.seekg(0, std::ios::beg);

    if(_array!=nullptr)
    {
        delete [] _array;
    }

    switch (type) {
    case GROUP_STRUCT:
    _array = new GroupStruct[_arraySize];
    break;
    case ALBUM_STRUCT:
        _array = new AlbumStruct[_arraySize];
    break;
    case SONG_STRUCT:
         _array = new SongStruct[_arraySize];
    break;
    default:
        std::cout << "UNABLE TO READ DATA IN MEMORY - TYPE PROBLEM" << std::endl;
        _array = nullptr;
        _arraySize = 0;
        return;
        break;
    }

    ebnFile.read((char*)_array, _arraySize*getStructSize());

    std::cout << "array is loaded for " << _arraySize << " elements" << std::endl;

}

void StructProcessor::deleteData(char *where, char *what, char *container = NULL,  bool all, DeleteOption opt)
{
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }

    if(what==NULL||where==NULL)
    {
        std::cout << "-column or -what data are incorrect" << std::endl;
        return;
    }

    std::cout << "StructProcessor::deleteData ";
    loadDataInMemory();
    ushort match;
    ushort id;
	ushort ids;
    countMatches(what,match,id);
    if(match==1){   ///one match found
		switch (type)
		{
		case GROUP_STRUCT:
			std::cout << "trying to remove group" << std::endl;
			fileManagerCallbackPtr_demandForeignIDsToDelete(getProcessorType(), ids, id);
			if (ids > 0) {
				if (!all && (opt != DELETE_HARD || opt != DELETE_SOFT))
					std::cout << "UNABLE TO DELETE GROUP " << what << " - it has " << ids << " albums" << std::endl;
				else {
					std::cout <<Color::FG_RED << "not empty group " <<Color::RESET<< id << " " << ids << std::endl;
					fileManagerCallbackPtr_demandDeletionByID(getProcessorType(), id);
					if (ebnFile.is_open())   ///in case if file is no existed
					{
						ebnFile.close();
                        ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
						for (int var = 0; var < _arraySize; ++var) {
							if (strcmp(_UseStruct(GroupStruct, _array)[var]._name, what))
								ebnFile.write(reinterpret_cast<char*>(&_UseStruct(GroupStruct, _array)[var]), getStructSize());
							else
								std::cout << "\tdeleting group (delete data) " << _UseStruct(GroupStruct, _array)[var]._name << std::endl;
						}
					}
				}
			}
			else {
				std::cout << "empty group" << std::endl;
				if (ebnFile.is_open())   ///in case if file is no existed
				{
					ebnFile.close();
					ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
					for (int var = 0; var < _arraySize; ++var) {
						if (strcmp(_UseStruct(GroupStruct, _array)[var]._name, what))
							ebnFile.write(reinterpret_cast<char*>(&_UseStruct(GroupStruct, _array)[var]), getStructSize());
						else
							std::cout << "\tdeleting group(by name) " << _UseStruct(GroupStruct, _array)[var]._name << std::endl;
					}
				}
			}		
			break;
		case ALBUM_STRUCT:{
            std::cout << "trying to remove album " << std::endl;        
            fileManagerCallbackPtr_demandForeignIDsToDelete(getProcessorType(),ids,id);
            if(ids>0)
            {
                if(!all&&(opt!=DELETE_HARD||opt!=DELETE_SOFT))
                    std::cout << "UNABLE TO DELETE ALBUM " << what << " - it has " << ids << " songs" << std::endl;
                else{
                    fileManagerCallbackPtr_demandDeletionByID(getProcessorType(),id);
                    loadDataInMemory();
                    if (ebnFile.is_open())   ///in case if file is no existed
                    {
                        ebnFile.close();
                        ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
                        for (int var = 0; var < _arraySize; ++var) {
                            if(strcmp(_UseStruct(AlbumStruct, _array)[var]._name,what))
                                ebnFile.write(reinterpret_cast<char*>(&_UseStruct(AlbumStruct, _array)[var]), getStructSize());
                            else
                                std::cout << "\tdeleting album(by name) " << _UseStruct(AlbumStruct, _array)[var]._name << std::endl;
                        }
                    }
                }
            }
            else{
                if (ebnFile.is_open())   ///in case if file is no existed
                {
                    ebnFile.close();
                    ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
                    for (int var = 0; var < _arraySize; ++var) {
                        if(strcmp(_UseStruct(AlbumStruct, _array)[var]._name,what))
                            ebnFile.write(reinterpret_cast<char*>(&_UseStruct(AlbumStruct, _array)[var]), getStructSize());
                        else
                            std::cout << "\tdeleting album (by name) " << _UseStruct(AlbumStruct, _array)[var]._name << std::endl;
                    }
                }
            }
        }
			break;
		case SONG_STRUCT:
            loadDataInMemory();
            if (ebnFile.is_open())   ///in case if file is no existed
            {
                ebnFile.close();
                ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
                for (int var = 0; var < _arraySize; ++var) {
                    if(strcmp(_UseStruct(SongStruct, _array)[var]._name,what))
                        ebnFile.write(reinterpret_cast<char*>(&_UseStruct(SongStruct, _array)[var]), getStructSize());
                    else
                        std::cout << "\tdeleting song(by name) " << _UseStruct(SongStruct, _array)[var]._name << std::endl;
                }
            }
			break;
		default:
			break;
	    }
        std::cout << "ONLY ONE MATCH FOUND ===> deleting" << std::endl;
    }
    else if(match>1){  ///multiple matches found  ///only for songs
        switch (type)
        {
        case SONG_STRUCT:
            loadDataInMemory();
            if (ebnFile.is_open())   ///in case if file is no existed
            {
                ushort id;
                fileManagerCallbackPtr_demandForeignID(id, container, getProcessorType());
                if(id==0)
                {
                    std::cout << "UNABLE TO LOCATE CONTAINER" << std::endl;
                    return;
                }
                ebnFile.close();
                ebnFile.open(fileName, std::ios::in | std::ios::out | std::ios::trunc);
                for (int var = 0; var < _arraySize; ++var) {
                    if(!strcmp(_UseStruct(SongStruct, _array)[var]._name,what)&&(id==_UseStruct(SongStruct, _array)[var]._foreignID))
                        std::cout << "\tdeleting song(by name and container) " << _UseStruct(SongStruct, _array)[var] << std::endl;
                    else
                        ebnFile.write(reinterpret_cast<char*>(&_UseStruct(SongStruct,_array)[var]), getStructSize());
                }
            }
            break;
        default:
            break;
        }
    }
    else{
        std::cout << "UNABLE TO DELETE ITEM << 0 matches found" << std::endl;
    }

}

void StructProcessor::searchForForeignIDfromReceivedName(const char* _name, ushort &id)
{

	if (!ebnFile.is_open())
	{
		system_cout << getProcessorType();
		critical_cout << " file not opened\n" << Color::RESET;
		return;
	}

    id = 0;

    if(_name==NULL){
        system_cout << getProcessorType();
        critical_cout << " container is null\n" << Color::RESET;
        return;
    }

	BaseStruct *bSt;
	initStruct(bSt);

	ebnFile.clear();  ///necessary to reach  0 pos after eof
	ebnFile.seekg(std::ios::beg);

	switch (type)
	{
    case GROUP_STRUCT:
		while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(GroupStruct, bSt)), getStructSize())){
			if (!strcmp(_UseStruct(GroupStruct, bSt)->_name, _name)) {
				id = _UseStruct(GroupStruct, bSt)->ID;
			}
		}
		break;
	case ALBUM_STRUCT:
		while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(AlbumStruct, bSt)), getStructSize()))
			if (!strcmp(_UseStruct(AlbumStruct, bSt)->_name, _name)) {
				id = _UseStruct(AlbumStruct, bSt)->ID;
			}
		break;
	default:
        std::cout << "StructProcessor::searchForForeignIDfromReceivedName: state - " << Color::FG_RED <<"FAIL "
			<<Color::RESET<< std::endl;
		id = 0;
		break;
	}

	delete bSt;

}

void StructProcessor::printStructEntriesFromFileOrMemory(char *type)
{
    if (!ebnFile.is_open())
    {
        system_cout << getProcessorType();
        critical_cout << " file not opened\n" << Color::RESET;
        return;
    }

    if(!strcmp(type,"--file")){

        std::cout << "Printing " <<Color::FG_CYAN << getProcessorType() << Color::RESET <<" file content" << std::endl;

        BaseStruct *bSt;
        initStruct(bSt);

        ebnFile.clear();  ///necessary to reach  0 pos after eof
        ebnFile.seekg(std::ios::beg);
        switch (this->type)
        {
        case GROUP_STRUCT:
            std::cout << std::setfill('-') << std::setw(6) << "ID" << std::setfill('-') << std::setw(21) << "name"
               << std::setfill('-') << std::setw(21) << "country"<< std::setfill('-') << std::setw(21) << "year"
               << std::setfill('-') << std::setw(6) << "f_ID" << "\n";
            while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(GroupStruct, bSt)), getStructSize()))
                std::cout << *_UseStruct(GroupStruct, bSt) << std::endl;
            break;
        case ALBUM_STRUCT:
            std::cout << std::setfill('-') << std::setw(6) << "ID" << std::setfill('-') << std::setw(21) << "name"
               << std::setfill('-') << std::setw(11) << "count" << std::setfill('-') << std::setw(11) <<"year"
               << std::setfill('-') << std::setw(6) << "f_ID"<< "\n";
            while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(AlbumStruct, bSt)), getStructSize()))
                std::cout << *_UseStruct(AlbumStruct, bSt) << std::endl;
            break;
        case SONG_STRUCT:
            std::cout << std::setfill('-') << std::setw(6) << "ID" << std::setfill('-') << std::setw(21) << "name"
               << std::setfill('-') << std::setw(11) << "duration"<< std::setfill('-') << std::setw(6) << "f_ID"<< "\n";
            while (ebnFile.read(reinterpret_cast<char*>(&*_UseStruct(SongStruct, bSt)), getStructSize()))
                std::cout << *_UseStruct(SongStruct, bSt) << std::endl;
            break;
        default:
            std::cout << "StructProcessor::printStructEntries(): state - " << Color::FG_RED << "FAIL "
                << Color::RESET << std::endl;
            break;
        }

        delete bSt;
    }
    else
    {
        loadDataInMemory();
        switch (this->type)
        {
        case GROUP_STRUCT:
            for (int var = 0; var < _arraySize; ++var)
                std::cout << _UseStruct(GroupStruct, _array)[var] << std::endl;
            break;
        case ALBUM_STRUCT:
            for (int var = 0; var < _arraySize; ++var)
                std::cout << _UseStruct(AlbumStruct, _array)[var] << std::endl;
            break;
        case SONG_STRUCT:
            for (int var = 0; var < _arraySize; ++var)
                std::cout << _UseStruct(SongStruct, _array)[var] << std::endl;
            break;
        default:
            std::cout << "StructProcessor::printStructEntries(): state - " << Color::FG_RED << "FAIL "
                << Color::RESET << std::endl;
            break;
        }
    }
}
