#ifndef STRUCTPROCESSOR_H
#define STRUCTPROCESSOR_H

#include "structs.h"
#include  "ostreamcolor/modifier.h"

#include <fstream>
#include <string>

#include <functional>

#include <vector>

typedef enum 
{
	BASE_STUCT,
    GROUP_STRUCT,
	ALBUM_STRUCT,
	SONG_STRUCT
}StructType;

typedef enum{
    NONE,
    DELETE_SOFT,
    DELETE_HARD
}DeleteOption;

class StructProcessor
{
public:
    /**
     * @brief StructProcessor
     * initializing processor with allowed type
     * @param type type from StructType
     */
    StructProcessor(StructType type = StructType::BASE_STUCT);
    void *myStruct;
    /**
     * @brief getProcessorType return current processor struct type
     * if something goes wrong it will return NULL
     * @return processor type
     */
    const char *getProcessorType();
    void addEntry(int argc, char **argv);
    void printStructEntriesFromFileOrMemory(char *type);

    inline void setCallbackFunction(std::function<void(ushort &,char*,const char*)>  func) { fileManagerCallbackPtr_demandForeignID = func; }
    inline void setCallbackFunction(std::function<void(const char*,ushort &,ushort)> func) { fileManagerCallbackPtr_demandForeignIDsToDelete = func; }
    inline void setCallbackFunction(std::function<void(const char*,ushort)> func) { fileManagerCallbackPtr_demandDeletionByID = func; }
    inline void setCallbackFunction(std::function<void(char*,char*,char *,char *,std::vector<ushort> &)> func) {fileManagerCallback_demandForeignIDsToContainer = func;}

    /**
     * @brief searchForForeignIDfromReceivedName
     * searche for existing id from table
     * table is choosed automatically by filemanager
     * @param groupName
     */
	void searchForForeignIDfromReceivedName(const char *groupName, ushort &id);
    /**
     * @brief loadDataInMemory
     */
    void loadDataInMemory();
    /**
     * @brief deleteData
     * @param where column, where we search @what
     * @param what value to delete
     * @param container determinant(to delete right item)
     * @param all if all is true, than tries to delete all items in table by @what
     * @param opt choose what to do with unreferenced items
     */
    void deleteData(char *where, char *what, char *container, bool all = false, DeleteOption opt = NONE);
    /**
     * @brief deleteByID
     * @param ids
     * @param id
     */
    void deleteByID(ushort id);
    /**
     * @brief getIDsCount
     * @param id
     */
    void getIDsCount(ushort &, ushort id);
    /**
     * @brief editEntry
     * @param argc
     * @param argv
     */
    void editEntry(int argc, char **argv);
    /**
     * @brief sortValues
     * @param what
     * @param where
     * @param descending
     */
    void sortValues(char *what, char *where, bool descending = true);
    void showValuesByCondition(char *what, char* condition, char *compare, char *container = nullptr, std::vector<ushort> *vector = NULL);
private:
    void initFile();
    void initStruct(BaseStruct *&_struct);
    /**
     * @brief getStructSize
     * returns structure size based on processor type
     * @return
     * in case, if something wrong happens(it realy dificult)
     * returns -1;
     */
    std::size_t getStructSize();
    void getMaxID(ushort &id);
    bool isNameUnical(BaseStruct *_struct);

	StructType type;
    BaseStruct *mainStruct;
    std::fstream ebnFile;
    std::string fileName;

    std::function<void(ushort &,char*,const char*)> fileManagerCallbackPtr_demandForeignID;
    std::function<void(const char*,ushort &,ushort)> fileManagerCallbackPtr_demandForeignIDsToDelete;
    std::function<void(const char*,ushort)> fileManagerCallbackPtr_demandDeletionByID;
    std::function<void(char*,char *,char*,char*,std::vector<ushort> &)> fileManagerCallback_demandForeignIDsToContainer;

    const char *_processorType;

    unsigned short _arraySize;
    BaseStruct *_array;

    /**
     * @brief countMatches
     * @param what
     * @param match
     * @param id
     */
    void countMatches(char *what, ushort &match, ushort &id);
};

#endif // STRUCTPROCESSOR_H
