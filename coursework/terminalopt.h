#ifndef TERMINALOPT_H
#define TERMINALOPT_H


class TerminalOptions
{
public:
    inline TerminalOptions(char *type){this->type = type;}
    /// set
    inline void setThreadID(long long id){threadID = id;}
	inline void setHistorySize(unsigned short hisSize) { historySize = hisSize; }
    /// get
    inline long long getThreadID(){return threadID;}
    inline const char* getType(){return type;}
	inline unsigned short getHistorySize() { return historySize; }
private:
    char *type;
    long long threadID;
	unsigned short historySize;
};


#endif // TERMINALOPT_H
