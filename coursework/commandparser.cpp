#include "commandparser.h"

#include  <iostream>
#include  <string> ///need to cout string
#include  <string.h>  ///gcc required for strlen

//insert --group -name test -genre rock -country latvia -year 1997
//insert --group -name "test test2" -genre rock -country latvia -year 1997

CommandParser::CommandParser()
{
	commands = NULL;
	singleCommand = NULL;
}


CommandParser::~CommandParser()
{
}

void CommandParser::processString(std::string command)
{
	std::cout << "Parsing received string: " << Color::FG_BLUE << command << Color::RESET << std::endl;
	if (!isQuotesCountEven(command)) {
		std::cout << Color::FG_RED << "QUOTES COUNT IS NOT EVEN" << Color::RESET << std::endl;
		//return;
	}
	else{
		if (commands != NULL)
		{
			for (unsigned int i = 0; i < commandsCount; i++) {
				free(commands[i]);
			}
			free(commands);
			commands = NULL;
			singleCommand = NULL;
		}
		char *tSingleCommand = NULL;
		char **tCommands = NULL;
		bool commandStarted = false;
		bool wordInQuete = false;
		unsigned int singleCommandCount = 0;
		commandsCount = 0;
		unsigned int i = 0;
		while (i < command.size())
		{
			if (command[i] == '"')
				wordInQuete = (!wordInQuete);
			if (command[i] == ' ' && !wordInQuete && tSingleCommand != NULL && singleCommand != NULL) {
				if (strlen(singleCommand)) {
					insertNewCommandInList(tCommands, singleCommandCount);
					singleCommand = NULL;
				}

				singleCommandCount = 0;

				commandStarted = false;
				if (i + 1 < command.size()) {
					commandStarted = (command[i + 1] == ' ');
				}
			}
			else {
				commandStarted = true;
			}
			if (((commandStarted&&command[i] != ' ' && !wordInQuete) || (wordInQuete))
				&& (command[i] != '"')) {
				singleCommandCount++;
				tSingleCommand = (char*)realloc(singleCommand, singleCommandCount * sizeof(char)+1);
				if (tSingleCommand != NULL) {
					singleCommand = tSingleCommand;
					singleCommand[singleCommandCount - 1] = command[i];
				}
				else {
					free(singleCommand);
					puts("Error (re)allocating memory");
					exit(1);
				}
			}
			i++;
			if (i == command.size())
			{
				if (strlen(singleCommand)) {
					insertNewCommandInList(tCommands, singleCommandCount);
				}
			}
		}
		dispatcherCallbackPtr(commandsCount, commands);
	}
	//std::cout << singleCommand << " " << commandsCount<< std::endl;
}

bool CommandParser::isQuotesCountEven(const std::string line)
{
	int quetesCount = 0;
	for (auto it = line.begin();it!=line.end();++it){
		if ((*it) == '"')
			quetesCount++;
	}
    return(!(quetesCount % 2));
}

void CommandParser::insertNewCommandInList(char **tCommands, unsigned int &count)
{
    singleCommand[count] = '\0';
    commandsCount++;
    tCommands = (char**)realloc(commands, commandsCount * sizeof(char *));
    if (tCommands != NULL) {
        commands = tCommands;
        commands[commandsCount - 1] = singleCommand;
    }
    else {
        free(commands);
        puts("Error (re)allocating memory");
        exit(1);
    }
}
