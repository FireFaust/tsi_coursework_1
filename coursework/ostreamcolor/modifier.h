#ifndef MODIFIER_H
#define MODIFIER_H

/**
* @brief define information!
* In order to use colored output in VC
* define VC_ON. To do that open
* Project Settings -> C/C++ -> Preprocessor -> Preprocessor
* and add another define
*/

#if defined(UNIX) || defined(__unix__) || defined(LINUX) || defined(__linux__) || defined(__linux)
#include <termios.h>
#include <unistd.h>
#include <stdio.h>

static int __attribute__((unused)) getch() {
	struct termios oldt, newt;
	int ch;
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	ch = getchar();
	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	return ch;
}
#elif defined(_WIN64) || defined(_WIN32)
#if (_MSC_VER>=1800)
#include <windows.h>
#define MAGENTA_FR 5
#define CYAN_FR 3
#endif
#include <conio.h>
#define getch _getch
#endif

#include <ostream>
/**
* used for colored output (for now only in iostream
*/
namespace Color {
	/**
	* @brief The Code enum
	* list of available colors
	*/
	enum Code {
		FG_BLACK = 30,
		FG_RED = 31,
		FG_GREEN = 32,
		FG_YELLOW = 33,
		FG_BLUE = 34,
		FG_MAGENTA = 35,
		FG_CYAN = 36,
		FG_LIGHT_GRAY = 37,
		FG_DARK_GRAY = 90,
		FG_LIGHT_RED = 91,
		FG_LIGHT_green = 92,
		FG_LIGHT_YELLOW = 93,
		FG_LIGHT_BLUE = 94,
		FG_LIGHT_MAGENTA = 95,
		FG_LIGHT_CYAN = 96,
		FG_WHITE = 97,
		FG_DEFAULT = 39,
		BG_RED = 41,
		BG_GREEN = 42,
		BG_YELLOW = 43,
		BG_BLUE = 44,
		BG_MAGENTA = 45,
		BG_CYAN = 46,
		BG_LIGHT_GRAY = 47,
		BG_DARK_GRAY = 100,
		BG_LIGHT_RED = 101,
		BG_LIGHT_GREEN = 102,
		BG_LIGHT_YELLOW = 103,
		BG_LIGHT_BLUE = 104,
		BG_LIGHT_MAGENTA = 105,
		BG_LIGHT_CYAN = 106,
		BG_WHITE = 107,
		BG_DEFAULT = 49,
		ST_BOLD = 1,
		ST_FAINT = 2,
		ST_ITALIC = 3,
		ST_UNDERLANE = 4,
		RESET = 0
	};
	/**
	* @brief operator << operator to overload
	* @param os std::ostream
	* @param code code from Code enum group
	* @return std::ostream and color value (colored text)
	*/
	inline std::ostream& operator<<(std::ostream& os, const Code& code) {
#if defined(UNIX) || defined(__unix__) || defined(LINUX) || defined(__linux__) || defined(__linux)
		return os << "\033[" << static_cast<int>(code) << "m";
#elif defined(_WIN64) || defined(_WIN32)
#if (_MSC_VER>=1800)
		HANDLE hStdout;
		switch (code)
		{
		case FG_BLUE:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE | FOREGROUND_INTENSITY);
			break;
		case FG_RED:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_INTENSITY);
			break;
		case FG_GREEN:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
			break;
		case FG_YELLOW:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY);
			break;
		case FG_MAGENTA:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, MAGENTA_FR | FOREGROUND_INTENSITY);
			break;
		case FG_CYAN:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout, CYAN_FR | FOREGROUND_INTENSITY);
			break;
		case FG_DEFAULT:
		case RESET:
			hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hStdout,
				FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			break;
		default:
			return os;
			break;
		}
#endif // _MSC_VER
		return os;
#endif
	}
	/**
	* @brief The Modifier class
	* second oportunity to use colored output
	*/
	class Modifier {
		Code code;
		bool show;
	public:
		Modifier(Code pCode, bool pShow = true) : code(pCode), show(pShow) {}
		/**
		* @brief operator <<operator to overload
		* @param os std::ostream
		* @param mod Modifier class
		* @return std::ostream and color value (colored text)
		*/
		friend std::ostream& operator<<(std::ostream& os, const Modifier& mod) {
			if (mod.show)
				return os << "\033[" << mod.code << "m";
			else
				return os;
		}
	};
}
#endif // MODIFIER
