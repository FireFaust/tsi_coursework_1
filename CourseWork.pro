QT += core

CONFIG += c++11

TARGET = ConsoleFlow
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    coursework/main.cpp \
    coursework/dispatcher.cpp \
    coursework/consolereader.cpp \
    coursework/consoleinterface.cpp \
    #coursework/filemanager.cpp \
    coursework/commandparser.cpp \
    coursework/filemanager.cpp \
    coursework/structprocessor.cpp \
    coursework/helper.cpp

HEADERS += \
    coursework/terminalopt.h \
    coursework/dispatcher.h \
    coursework/consolereader.h \
    coursework/consoleinterface.h \
    ostreamcolor/modifier.h \
    #coursework/filemanager.h \
    coursework/commandparser.h \
    coursework/filemanager.h \
    coursework/structs.h \
    coursework/structprocessor.h \
    coursework/helper.h

DEFINES += VC_ON
